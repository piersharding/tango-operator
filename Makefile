KUBE_NAMESPACE = tango-operator-system
KUBE_REPORT_NAMESPACE ?= default
WEBHOOK_SERVICE_NAME = tango-operator-webhook-service
CERT_DIR = /tmp/k8s-webhook-server/serving-certs
TEMP_DIRECTORY := $(shell mktemp -d)
ROOT_DIR:=$(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))
LOG_LEVEL ?= INFO
HELM_CHARTS_TO_PUBLISH ?= tango-operator tango-operator-crd
HELM_CHARTS ?= $(HELM_CHARTS_TO_PUBLISH)
HELM_CHARTS_CHANNEL ?= release

# Produce CRDs that work back to Kubernetes 1.11 (no version conversion)
CRD_OPTIONS ?= "crd:trivialVersions=true"

# ENVTEST_K8S_VERSION refers to the version of kubebuilder assets to be downloaded by envtest binary.
# 1.19.2 is the last known one to work - see https://book.kubebuilder.io/reference/envtest.html
ENVTEST_K8S_VERSION = 1.19.2

# Controller runtime arguments
CONTROLLER_ARGS ?=
TEST_USE_EXISTING_CLUSTER ?= false

# Get the currently used golang install path (in GOPATH/bin, unless GOBIN is set)
ifneq (, $(shell which go))
ifeq (,$(shell go env GOBIN))
GOBIN=$(shell go env GOPATH)/bin
else
GOBIN=$(shell go env GOBIN)
endif
endif

# gitlab-runner configuration
GITLAB_JOB ?= compile ## gitlab-runner Job step to test
RDEBUG ?= ""
TIMEOUT = 86400
EXECUTOR ?= shell
CI_ENVIRONMENT_SLUG ?= development
CI_PIPELINE_ID ?= pipeline$(shell tr -c -d '0123456789abcdefghijklmnopqrstuvwxyz' </dev/urandom | dd bs=8 count=1 2>/dev/null;echo)
CI_JOB_ID ?= local
CI_PROJECT_ID ?= 21021099
GITLAB_API_REGISTRY=https://gitlab.com/api/v4/projects/$(CI_PROJECT_ID)/registry/repositories

CI_JOB_TOKEN ?=
GITLAB_TOKEN ?=
ifeq ($(strip $(GITLAB_TOKEN)),)
GITLAB_TOKEN_HEADER=JOB-TOKEN: $(CI_JOB_TOKEN)
else
GITLAB_TOKEN_HEADER=PRIVATE-TOKEN: $(GITLAB_TOKEN)
endif

GITLAB_USER ?= ""
CI_BUILD_TOKEN ?= ""
REGISTRY_TOKEN ?= ""
DOCKER_HOST ?= unix:///var/run/docker.sock
DOCKER_VOLUMES ?= /var/run/docker.sock:/var/run/docker.sock
CI_APPLICATION_TAG ?= $(shell git rev-parse --verify --short=8 HEAD)

-include .make/base.mk
-include .make/oci.mk
-include .make/helm.mk
-include .make/k8s.mk

-include PrivateRules.mak

# Image URL to use all building/pushing image targets
IMG ?= registry.gitlab.com/piersharding/tango-operator/tango-operator:$(VERSION)

CAR_OCI_REGISTRY_HOST=registry.gitlab.com/piersharding/tango-operator
OCI_IMAGES=tango-databaseds tango-db tango-controls
TAG=$(VERSION)

DSCONFIG_IMAGE ?= $(CAR_OCI_REGISTRY_HOST)/tango-controls:$(VERSION)
DS_IMAGE ?= $(DSCONFIG_IMAGE)

K8S_CHARTS = tango-operator

# Setting SHELL to bash allows bash commands to be executed by recipes.
# This is a requirement for 'setup-envtest.sh' in the test target.
# Options are set to exit when a recipe line exits non-zero or a piped command fails.
SHELL = /usr/bin/env bash -o pipefail

# turn off the shell flags for helm-lint
# ifneq ($(strip $(HELP_CURRENT_TARGET)),helm-lint)
# .SHELLFLAGS = -ec
# endif

.PHONY: all k8s show lint deploy delete logs describe namespace test clean run install reports help
.DEFAULT_GOAL := help

to-showtag:  ## show the version and suffix to be added to images
	@echo "$(VERSION)$(OCI_BUILD_SUFFIX)"

dotenv: ## set release variables - build release unless CI_COMMIT_TAG set
	@echo "RELEASE=$(VERSION)" > variables.env
	@TANGO_OPERATOR_ID=`curl -s --header "$(GITLAB_TOKEN_HEADER)" "$(GITLAB_API_REGISTRY)"  | jq -r '.[] | select(.name == "tango-operator") | .id'` && \
	echo "TANGO_OPERATOR_ID=$${TANGO_OPERATOR_ID}" >>variables.env
	@if [ -z "${CI_COMMIT_TAG}" ]; then \
	RELEASE_SUFFIX=".c${CI_COMMIT_SHORT_SHA}"; \
	else \
	RELEASE_SUFFIX=""; \
	fi; \
	echo "RELEASE_SUFFIX=$${RELEASE_SUFFIX}" >> variables.env
	@if [ -z "${CI_COMMIT_TAG}" ]; then \
	RELEASE_REF="${CI_COMMIT_SHORT_SHA}"; \
	else \
	RELEASE_REF="${CI_COMMIT_TAG}"; \
	fi; \
	echo "RELEASE_REF=$${RELEASE_REF}" >> variables.env
	cat variables.env

main-set-image-version:  ## update the image tags for dependent images
	@for i in ska-tango-examples ska-tango-images-tango-dsconfig ska-tango-images-tango-cpp ska-tango-images-tango-db ; do \
	export RELEASE_VERSION=`curl -k -s -X GET https://artefact.skao.int/v2/$${i}/tags/list | jq -r '.tags[]' | grep -v dirty | sort --version-sort | tail -1`; \
	echo "Latest tag for: $$i is $$RELEASE_VERSION"; \
	sed -i.x -e "s/\(artefact.skao.int\/$${i}:\).*/\1$${RELEASE_VERSION}\"/" main.go && \
	rm -f main.go.x; \
	sed -i.x -e "s/\(artefact.skao.int\/$${i}:\).*/\1$${RELEASE_VERSION}/" config/helmchart/config.yaml && \
	rm -f config/helmchart/config.yaml.x; \
	sed -i.x -e "s/\(artefact.skao.int\/$${i}:\).*/\1$${RELEASE_VERSION}/" Dockerfile.operator && \
	rm -f Dockerfile.operator.x; \
	done
	echo "Check the changes: "
	grep artefact.skao.int main.go config/helmchart/config.yaml Dockerfile.operator
	git diff --patch-with-stat main.go config/helmchart/config.yaml Dockerfile.operator

.PHONY: to-patch
to-patch: bump-patch-release helm-set-release helm-set-operator-version git-create-tag git-push-tag

.PHONY: to-minor
to-minor: bump-minor-release helm-set-release helm-set-operator-version git-create-tag git-push-tag

.PHONY: to-major
to-major: bump-major-release helm-set-release helm-set-operator-version git-create-tag git-push-tag

to-install-local:  ## helm install local chart
	export HELM_CACHE_HOME=$$(mktemp -d) && \
	echo "HELM CACHE DIR=$${HELM_CACHE_HOME}" && \
	helm install --repository-cache $${HELM_CACHE_HOME} --create-namespace --namespace to to ./charts/tango-operator && \
	rm -rf $${HELM_CACHE_HOME}

.PHONY: to-install
to-install:  ## Install CRDs and operator into the K8s cluster using charts
	helm repo list | grep "to  "
	helm repo update
	make k8s-install-chart K8S_UMBRELLA_CHART_PATH=to/tango-operator HELM_RELEASE=operator

.PHONY: to-uninstall
to-uninstall:  ## UnInstall CRDs and operator into the K8s cluster using charts
	make k8s-uninstall-chart HELM_RELEASE=operator

.PHONY: to-reinstall
to-reinstall: to-uninstall to-install

# all: build
all: manifests build  ## run all

##@ General

# The help target prints out all targets with their descriptions organized
# beneath their categories. The categories are represented by '##@' and the
# target descriptions by '##'. The awk commands is responsible for reading the
# entire set of makefiles included in this invocation, looking for lines of the
# file as xyz: ## something, and then pretty-format the target and help. Then,
# if there's a line with ##@ something, that gets pretty-printed as a category.
# More info on the usage of ANSI control characters for terminal formatting:
# https://en.wikipedia.org/wiki/ANSI_escape_code#SGR_parameters
# More info on the awk command:
# http://linuxcommand.org/lc3_adv_awk.php

# .PHONY: help
# help: ## Display this help.
# 	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n"} /^[a-zA-Z_0-9-]+:.*?##/ { printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

##@ Development

.PHONY: manifests
manifests: controller-gen ## Generate WebhookConfiguration, ClusterRole and CustomResourceDefinition objects.
	$(CONTROLLER_GEN) rbac:roleName=manager-role crd webhook paths="./..." output:crd:artifacts:config=config/crd/bases

.PHONY: generate
generate: controller-gen ## Generate code containing DeepCopy, DeepCopyInto, and DeepCopyObject method implementations.
	export GO111MODULE=on && $(CONTROLLER_GEN) object:headerFile="hack/boilerplate.go.txt" paths="./..."

deps: ## update all dependencies
	export GO111MODULE=on && go clean -modcache && go mod download

# go mod tidy -compat=1.18

cleandir:
	sudo rm -rf $(ROOT_DIR)/builds

rjob: cleandir ## run code standards check using gitlab-runner
	if [ -n "$(RDEBUG)" ]; then DEBUG_LEVEL=debug; else DEBUG_LEVEL=warn; fi && \
	gitlab-runner --log-level $${DEBUG_LEVEL} exec $(EXECUTOR) \
	--docker-privileged \
	 --docker-disable-cache=false \
	--docker-host $(DOCKER_HOST) \
	--docker-volumes  $(DOCKER_VOLUMES) \
	--docker-pull-policy always \
	--timeout $(TIMEOUT) \
    --env "DOCKER_HOST=$(DOCKER_HOST)" \
		--env "GITLAB_USER=$(GITLAB_USER)" \
		--env "REGISTRY_TOKEN=$(REGISTRY_TOKEN)" \
		--env "CI_BUILD_TOKEN=$(CI_BUILD_TOKEN)" \
		--env "TRACE=1" \
		--env "DEBUG=1" \
	$(GITLAB_JOB) || true

.PHONY: fmt
fmt: ## Run go fmt against code.
	go fmt ./...

.PHONY: vet
vet: ## Run go vet against code.
	go vet ./...



.PHONY: test
test: manifests generate fmt vet envtest ## Run tests.
	@rm -rf cover.* cover
	@mkdir -p cover
	LOG_LEVEL=$(LOG_LEVEL) TEST_USE_EXISTING_CLUSTER=$(TEST_USE_EXISTING_CLUSTER) \
	KUBEBUILDER_ASSETS="$(shell $(ENVTEST) use $(ENVTEST_K8S_VERSION) -p path)" \
	go test ./api/... ./controllers/... -v -coverprofile cover.out.tmp
	@cat cover.out.tmp | grep -v "zz_generated.deepcopy.go" > cover.out
	@go tool cover -html=cover.out -o cover/cover.html
	@cp cover.out.tmp cover/cover.out
	@go tool cover -func=cover.out | grep 'total:' | awk '{print $$3}'  | tr -d '%' > cover/total.txt
	@rm -f cover.out cover.out.tmp

##@ Build

.PHONY: build
build: generate fmt vet ## Build manager binary.
	go build -o bin/manager main.go

.PHONY: manager
manager: build

.PHONY: run
run: manifests generate fmt vet ## Run a controller from your host.
	LOG_LEVEL=$(LOG_LEVEL) go run ./main.go

.PHONY: docker-build
docker-build: test ## Build docker image with the manager.
	docker build -t ${IMG} -f Dockerfile.operator .

.PHONY: docker-push
docker-push: ## Push docker image with the manager.
	docker push ${IMG}

##@ Deployment

ifndef ignore-not-found
  ignore-not-found = false
endif

.PHONY: install
install: manifests generate kustomize ## Install CRDs into the K8s cluster specified in ~/.kube/config.
	$(KUSTOMIZE) build config/crd | kubectl apply -f -

.PHONY: uninstall
uninstall: manifests generate kustomize ## Uninstall CRDs from the K8s cluster specified in ~/.kube/config. Call with ignore-not-found=true to ignore resource not found errors during deletion.
	$(KUSTOMIZE) build config/crd | kubectl delete --ignore-not-found=$(ignore-not-found) -f - || true

.PHONY: reinstall
reinstall: uninstall install

.PHONY: deploy
deploy: manifests kustomize ## Deploy controller to the K8s cluster specified in ~/.kube/config.
	cd config/manager && $(KUSTOMIZE) edit set image controller=${IMG}
	$(KUSTOMIZE) build config/default | kubectl apply -f -

.PHONY: undeploy
undeploy: ## Undeploy controller from the K8s cluster specified in ~/.kube/config. Call with ignore-not-found=true to ignore resource not found errors during deletion.
	$(KUSTOMIZE) build config/default | kubectl delete --ignore-not-found=$(ignore-not-found) -f -

CONTROLLER_GEN = $(shell pwd)/bin/controller-gen
.PHONY: controller-gen
controller-gen: ## Download controller-gen locally if necessary.
	$(call go-get-tool,$(CONTROLLER_GEN),sigs.k8s.io/controller-tools/cmd/controller-gen@v0.10.0)

KUSTOMIZE = $(shell pwd)/bin/kustomize
.PHONY: kustomize
kustomize: ## Download kustomize locally if necessary.
	$(call go-get-tool,$(KUSTOMIZE),sigs.k8s.io/kustomize/kustomize/v4@v4.5.7)

ENVTEST = $(shell pwd)/bin/setup-envtest
.PHONY: envtest
envtest: ## Download envtest-setup locally if necessary.
	$(call go-get-tool,$(ENVTEST),sigs.k8s.io/controller-runtime/tools/setup-envtest@latest)

# go-get-tool will 'go get' any package $2 and install it to $1.
PROJECT_DIR := $(shell dirname $(abspath $(firstword $(MAKEFILE_LIST))))
define go-get-tool
@[ -f $(1) ] || { \
set -e ;\
unset GO111MODULE; \
TMP_DIR=$$(mktemp -d) ;\
cd $$TMP_DIR ;\
go mod init tmp ;\
echo "Downloading $(2) to $(PROJECT_DIR)/bin" ;\
GOBIN=$(PROJECT_DIR)/bin go install $(2) ;\
rm -rf $$TMP_DIR ;\
}
endef

HELMIFY = $(shell pwd)/bin/helmify
.PHONY: helmify
helmify:
	$(call go-get-tool,$(HELMIFY),github.com/arttor/helmify/cmd/helmify@v0.3.18)

helm-crd: manifests kustomize helmify
	$(KUSTOMIZE) build config/crd | $(HELMIFY) charts/tango-operator-crd

helm-operator: manifests kustomize helmify
	cd config/manager && $(KUSTOMIZE) edit set image controller=${IMG}
	$(KUSTOMIZE) build config/helmchart | $(HELMIFY) charts/tango-operator

helm: helm-crd helm-operator

helm-set-operator-version:
	@export RELEASE_VERSION=$$( cat .release | grep release | cut -d "=" -f 2 ) && \
	sed -i.x -e "N;s/\(repository: registry.gitlab.com.*tag:\).*/\1 $${RELEASE_VERSION}/;P;D" charts/tango-operator/values.yaml && \
	rm -f charts/tango-operator/values.yaml.x

oplogs: ## operator logs
	kubectl logs -n to $$(kubectl -n to get pods -l app.kubernetes.io/name=tango-operator -o name) -f


logs: ## show Helm chart POD logs
	@for i in `kubectl -n $(KUBE_NAMESPACE) get pods -l control-plane=controller-manager -o=name`; \
	do \
		echo "---------------------------------------------------"; \
		echo "Logs for $${i}"; \
		echo kubectl -n $(KUBE_NAMESPACE) logs $${i}; \
		echo kubectl -n $(KUBE_NAMESPACE) get $${i} -o jsonpath="{.spec.initContainers[*].name}"; \
		echo "---------------------------------------------------"; \
		for j in `kubectl -n $(KUBE_NAMESPACE) get $${i} -o jsonpath="{.spec.initContainers[*].name}"`; do \
			RES=`kubectl -n $(KUBE_NAMESPACE) logs $${i} -c $${j} 2>/dev/null`; \
			echo "initContainer: $${j}"; echo "$${RES}"; \
			echo "---------------------------------------------------";\
		done; \
		echo "Main Pod logs for $${i}"; \
		echo "---------------------------------------------------"; \
		for j in `kubectl -n $(KUBE_NAMESPACE) get $${i} -o jsonpath="{.spec.containers[*].name}"`; do \
			RES=`kubectl -n $(KUBE_NAMESPACE) logs $${i} -c $${j} 2>/dev/null`; \
			echo "Container: $${j}"; echo "$${RES}"; \
			echo "---------------------------------------------------";\
		done; \
		echo "---------------------------------------------------"; \
		echo ""; echo ""; echo ""; \
	done

describe: ## describe Pods executed from Helm chart
	@for i in `kubectl -n $(KUBE_NAMESPACE) get pods -l control-plane=controller-manager -o=name`; \
	do echo "---------------------------------------------------"; \
	echo "Describe for $${i}"; \
	echo kubectl -n $(KUBE_NAMESPACE) describe $${i}; \
	echo "---------------------------------------------------"; \
	kubectl -n $(KUBE_NAMESPACE) describe $${i}; \
	echo "---------------------------------------------------"; \
	echo ""; echo ""; echo ""; \
	done

tangodslogs: ## show Tango Device Server POD logs
	@for i in `kubectl -n $(KUBE_REPORT_NAMESPACE) get pods -l app.kubernetes.io/name=tango-device-server -o=name`; \
	do \
		echo "---------------------------------------------------"; \
		echo "Logs for $${i}"; \
		echo kubectl -n $(KUBE_REPORT_NAMESPACE) logs $${i}; \
		echo kubectl -n $(KUBE_REPORT_NAMESPACE) get $${i} -o jsonpath="{.spec.initContainers[*].name}"; \
		echo "---------------------------------------------------"; \
		for j in `kubectl -n $(KUBE_REPORT_NAMESPACE) get $${i} -o jsonpath="{.spec.initContainers[*].name}"`; do \
			RES=`kubectl -n $(KUBE_REPORT_NAMESPACE) logs $${i} -c $${j} 2>/dev/null`; \
			echo "initContainer: $${j}"; echo "$${RES}"; \
			echo "---------------------------------------------------";\
		done; \
		echo "Main Pod logs for $${i}"; \
		echo "---------------------------------------------------"; \
		for j in `kubectl -n $(KUBE_REPORT_NAMESPACE) get $${i} -o jsonpath="{.spec.containers[*].name}"`; do \
			RES=`kubectl -n $(KUBE_REPORT_NAMESPACE) logs $${i} -c $${j} 2>/dev/null`; \
			echo "Container: $${j}"; echo "$${RES}"; \
			echo "---------------------------------------------------";\
		done; \
		echo "---------------------------------------------------"; \
		echo ""; echo ""; echo ""; \
	done

testcerts:
	rm -rf $(CERT_DIR)
	mkdir -p $(CERT_DIR)
	openssl req -x509 -newkey rsa:2048 -keyout $(CERT_DIR)/tls.key -out $(CERT_DIR)/tls.crt -days 365 -nodes -subj "/CN=$(WEBHOOK_SERVICE_NAME).$(KUBE_NAMESPACE).svc"

certs:
	# from https://kubernetes.github.io/ingress-nginx/deploy/validating-webhook/
	rm -rf $(CERT_DIR)
	mkdir -p $(CERT_DIR)
	@echo "[req]\n" \
	"req_extensions = v3_req\n" \
	"distinguished_name = req_distinguished_name\n" \
	"[req_distinguished_name]\n" \
	"[ v3_req ]\n" \
	"basicConstraints = CA:FALSE\n" \
	"keyUsage = nonRepudiation, digitalSignature, keyEncipherment\n" \
	"extendedKeyUsage = serverAuth\n" \
	"subjectAltName = @alt_names\n" \
	"[alt_names]\n" \
	"DNS.1 = $(WEBHOOK_SERVICE_NAME)\n" \
	"DNS.2 = $(WEBHOOK_SERVICE_NAME).$(KUBE_NAMESPACE)\n" \
	"DNS.3 = $(WEBHOOK_SERVICE_NAME).$(KUBE_NAMESPACE).svc\n" \
	| sed 's/ //' > $(CERT_DIR)/csr.conf
	cat $(CERT_DIR)/csr.conf
	openssl genrsa -out $(CERT_DIR)/tls.key 2048
	openssl req -new -key $(CERT_DIR)/tls.key \
	-subj "/CN=$(WEBHOOK_SERVICE_NAME).$(KUBE_NAMESPACE).svc" \
	-out $(CERT_DIR)/server.csr \
	-config $(CERT_DIR)/csr.conf
	@echo \
	"apiVersion: certificates.k8s.io/v1beta1\n" \
	"kind: CertificateSigningRequest\n" \
	"metadata:\n" \
	"  name: $(WEBHOOK_SERVICE_NAME).$(KUBE_NAMESPACE).svc\n" \
	"spec:\n" \
	"  request: $$(cat $(CERT_DIR)/server.csr | base64 | tr -d '\n')\n" \
	"  usages:\n" \
	"  - digital signature\n" \
	"  - key encipherment\n" \
	"  - server auth\n" \
	| sed 's/^ //' > $(CERT_DIR)/approve.yaml
	ls -latr $(CERT_DIR)
	cat $(CERT_DIR)/approve.yaml
	kubectl delete -f $(CERT_DIR)/approve.yaml || true
	kubectl apply -f $(CERT_DIR)/approve.yaml
	sleep 3
	kubectl certificate approve $(WEBHOOK_SERVICE_NAME).$(KUBE_NAMESPACE).svc

getcert:
	while true; do \
	STATUS=$$(kubectl get csr $(WEBHOOK_SERVICE_NAME).$(KUBE_NAMESPACE).svc  -o jsonpath='{.status.conditions[].type}'); \
	if [ "$${STATUS}" = "Approved" ]; then break; fi; \
	echo "Status is: $${STATUS} - sleeping"; \
	sleep 10; \
	done
	SERVER_CERT=$$(kubectl get csr $(WEBHOOK_SERVICE_NAME).$(KUBE_NAMESPACE).svc  -o jsonpath='{.status.certificate}') && \
	echo $${SERVER_CERT} | openssl base64 -d -A -out $(CERT_DIR)/tls.crt
	rm -rf config/webhook/secret/tls.*
	cp $(CERT_DIR)/tls.crt $(CERT_DIR)/tls.key config/webhook/secret/

secret: namespace
	kubectl delete secret webhook-server-cert -n $(KUBE_NAMESPACE) || true
	kubectl create secret generic webhook-server-cert \
	--from-file=tls.key=$(CERT_DIR)/tls.key \
	--from-file=tls.crt=$(CERT_DIR)/tls.crt \
	-n $(KUBE_NAMESPACE)
