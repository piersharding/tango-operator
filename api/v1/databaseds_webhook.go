/*

Copyright 2022 Piers Harding
Copyright 2022 SKA Observatory

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
// +kubebuilder:docs-gen:collapse=Apache License

// Go imports
package v1

import (
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	validationutils "k8s.io/apimachinery/pkg/util/validation"
	"k8s.io/apimachinery/pkg/util/validation/field"

	ctrl "sigs.k8s.io/controller-runtime"

	"sigs.k8s.io/controller-runtime/pkg/webhook"
)

var databasedslog = ctrl.Log.WithName("databaseds-resource")

func (r *DatabaseDS) SetupWebhookWithManager(mgr ctrl.Manager) error {
	databasedslog.Info("Activating Webhook")
	return ctrl.NewWebhookManagedBy(mgr).
		For(r).
		Complete()
}

// +kubebuilder:webhook:path=/mutate-analytics-tango-controls-org-v1-databaseds,mutating=true,failurePolicy=fail,groups=tango.tango-controls.org,resources=databasedss,verbs=create;update,versions=v1,name=mdatabaseds.tango-controls.org,admissionReviewVersions=v1,sideEffects=None

var _ webhook.Defaulter = &DatabaseDS{}

// Default implements webhook.Defaulter so a webhook will be registered for the type
func (r *DatabaseDS) Default() {
	databasedslog.Info("default", "name", r.Name)

	if r.Spec.DDSImage == "" {
		r.Spec.DDSImage = "artefact.skao.int/ska-tango-images-tango-databaseds-alpine:0.3.0"
	}

	if r.Spec.DBImage == "" {
		r.Spec.DBImage = "artefact.skao.int/ska-tango-images-tango-db:10.4.14"
	}

	if r.Spec.ImagePullPolicy == "" {
		r.Spec.ImagePullPolicy = "IfNotPresent"
	}

	if r.Spec.ClusterDomain == "" {
		r.Spec.ClusterDomain = "cluster.local"
	}

	if r.Spec.Port == 0 {
		r.Spec.Port = 10000
	}

	if r.Spec.DatabaseDS == "" {
		r.Spec.DatabaseDS = r.Name
	}

	// if r.Spec.Daemon == nil {
	// 	r.Spec.Daemon = new(bool)
	// }

	// if r.Spec.Replicas == nil {
	// 	r.Spec.Replicas = new(int32)
	// 	*r.Spec.Replicas = 1
	// }

	// if r.Spec.Replicas == 0 {
	// 	r.Spec.Replicas = int32(5)
	// }
}

// TODO(user): change verbs to "verbs=create;update;delete" if you want to enable deletion validation.
// +kubebuilder:webhook:verbs=create;update,path=/validate-analytics-tango-controls-org-v1-databaseds,mutating=false,failurePolicy=fail,groups=tango.tango-controls.org,resources=databasedss,versions=v1,name=vdatabaseds.tango-controls.org,admissionReviewVersions=v1,sideEffects=None

var _ webhook.Validator = &DatabaseDS{}

// ValidateCreate implements webhook.Validator so a webhook will be registered for the type
func (r *DatabaseDS) ValidateCreate() error {
	databasedslog.Info("validate create", "name", r.Name)

	return r.validateDatabaseDS()
}

// ValidateUpdate implements webhook.Validator so a webhook will be registered for the type
func (r *DatabaseDS) ValidateUpdate(old runtime.Object) error {
	databasedslog.Info("validate update", "name", r.Name)

	return r.validateDatabaseDS()
}

// ValidateDelete implements webhook.Validator so a webhook will be registered for the type
func (r *DatabaseDS) ValidateDelete() error {
	databasedslog.Info("validate delete", "name", r.Name)

	// TODO(user): fill in your validation logic upon object deletion.
	return nil
}

func (r *DatabaseDS) validateDatabaseDS() error {
	var allErrs field.ErrorList
	if err := r.validateDatabaseDSName(); err != nil {
		allErrs = append(allErrs, err)
	}
	if err := r.validateDatabaseDSSpec(); err != nil {
		allErrs = append(allErrs, err)
	}
	if len(allErrs) == 0 {
		return nil
	}

	return apierrors.NewInvalid(
		schema.GroupKind{Group: "tango.tango-controls.org", Kind: "DatabaseDS"},
		r.Name, allErrs)
}

func (r *DatabaseDS) validateDatabaseDSSpec() *field.Error {
	// The field helpers from the kubernetes API machinery help us return nicely
	// structured validation errors.
	// return validateScheduleFormat(
	// 	r.Spec.Schedule,
	// 	field.NewPath("spec").Child("schedule"))
	return nil
}

func (r *DatabaseDS) validateDatabaseDSName() *field.Error {
	if len(r.ObjectMeta.Name) > validationutils.DNS1035LabelMaxLength-11 {
		// The job name length is 63 character like all Kubernetes objects
		// (which must fit in a DNS subdomain). The cronjob controller appends
		// a 11-character suffix to the cronjob (`-$TIMESTAMP`) when creating
		// a job. The job name length limit is 63 characters. Therefore cronjob
		// names must have length <= 63-11=52. If we don't validate this here,
		// then job creation will fail later.
		return field.Invalid(field.NewPath("metadata").Child("name"), r.Name, "must be no more than 52 characters")
	}
	return nil
}
