/*

Copyright 2022 Piers Harding
Copyright 2022 SKA Observatory

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package v1

import (
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// DeviceServerSpec defines the desired state of DeviceServer
type DeviceServerSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	//+kubebuilder:validation:Default=false

	// Disable Network Policies
	//+optional
	DisablePolicies bool `json:"disablepolicies,omitempty"`

	// DeviceServer databaseds resource name that is the Tango DatabaseDS the DeviceServer will run against: mandatory
	DatabaseDS string `json:"databaseds"`

	// External DeviceServer DNS name or IP address - must be supplied if ExternalDeviceServer is set
	// Make the deviceServer an external device only - this will only deploy the Service type ExternalName and pass DS config to TangoDB
	// +optional
	ExternalDeviceServer string `json:"externalDeviceServer,omitempty"`

	//+kubebuilder:validation:Default=false

	// Enable Load Balancer for external access to DatabaseDS
	// +optional
	EnableLoadBalancer bool `json:"enableLoadBalancer,omitempty"`

	//+kubebuilder:validation:Default=false

	// Specify Direct Connection to TANGO_HOST in DatabaseDS config option
	//+optional
	DirectConnection bool `json:"directConnection,omitempty"`

	//+kubebuilder:validation:Minimum=0

	// Number of workers to spawn - default will be 5
	//+optional
	// Replicas int32 `json:"replicas,omitempty"`

	// DeviceServer config for this job - FQ file name, HTTP URL, or full config body .json: mandatory
	Config string `json:"config"`

	// DeviceServer script for this job - FQ file name, HTTP URL, or full script body either .py or .ipynb: mandatory
	Script string `json:"script"`

	//+kubebuilder:validation:MinLength=0

	// Arguments to pass to app boot - default:
	Args string `json:"args,omitempty"`

	//+kubebuilder:validation:MinLength=0
	//+kubebuilder:validation:Default=artefact.skao.int/ska-tango-images-tango-pytango-alpine:0.3.1

	// Source image to deploy cluster from - default: artefact.skao.int/ska-tango-images-tango-pytango-alpine:0.3.1
	Image string `json:"image,omitempty"`

	// Pull Policy for image - default: IfNotPresent
	//+optional
	ImagePullPolicy string `json:"imagePullPolicy,omitempty"`

	// Specifies the DeviceServers that must be started before this one.
	//+optional
	DependsOn []string `json:"dependsOn,omitempty"`

	// OmniOrb Port default 45450
	//+kubebuilder:validation:Minimum=0
	//+kubebuilder:default=45450
	//+optional
	OrbPort int32 `json:"orbport,omitempty"`

	// ZMQ Heartbeat Port default 45460
	//+kubebuilder:validation:Minimum=0
	//+kubebuilder:default=45460
	//+optional
	HeartbeatPort int32 `json:"heartbeatport,omitempty"`

	// Kubernetes cluster domain default cluster.local
	//+kubebuilder:default=cluster.local
	//+optional
	ClusterDomain string `json:"clusterDomain,omitempty"`

	// ZMQ Event Port default 45470
	//+kubebuilder:validation:Minimum=0
	//+kubebuilder:default=45470
	//+optional
	EventPort int32 `json:"eventport,omitempty"`

	// Specifies the Volumes.
	//+optional
	Volumes []corev1.Volume `json:"volumes,omitempty"`

	// Specifies the VolumeMounts.
	//+optional
	VolumeMounts []corev1.VolumeMount `json:"volumeMounts,omitempty"`

	// Specifies the Environment variables.
	//+optional
	Env []corev1.EnvVar `json:"env,omitempty"`

	// Specifies the Pull Secrets.
	//+optional
	PullSecrets []corev1.LocalObjectReference `json:"imagePullSecrets,omitempty"`

	// Specifies the NodeSelector configuration.
	//+optional
	NodeSelector map[string]string `json:"nodeSelector,omitempty"`

	// Specifies the Affinity configuration.
	//+optional
	Affinity *corev1.Affinity `json:"affinity,omitempty"`

	// Specifies the Toleration configuration.
	//+optional
	Tolerations []corev1.Toleration `json:"tolerations,omitempty"`

	// Specifies the Environment variables.
	//+optional
	Resources *corev1.ResourceRequirements `json:"resources,omitempty"`
}

// DeviceServerDeploymentSpec - shared structure of configurable attributes
// type DeviceServerDeploymentSpec struct {
// 	// Specifies the Volumes.
// 	//+optional
// 	Volumes []corev1.Volume `json:"volumes,omitempty"`

// 	// Specifies the VolumeMounts.
// 	//+optional
// 	VolumeMounts []corev1.VolumeMount `json:"volumeMounts,omitempty"`

// 	// Specifies the Environment variables.
// 	//+optional
// 	Env []corev1.EnvVar `json:"env,omitempty"`

// 	// Specifies the Pull Secrets.
// 	//+optional
// 	PullSecrets []corev1.LocalObjectReference `json:"imagePullSecrets,omitempty"`

// 	// Specifies the NodeSelector configuration.
// 	//+optional
// 	NodeSelector map[string]string `json:"nodeSelector,omitempty"`

// 	// Specifies the Affinity configuration.
// 	//+optional
// 	Affinity *corev1.Affinity `json:"affinity,omitempty"`

// 	// Specifies the Toleration configuration.
// 	//+optional
// 	Tolerations []corev1.Toleration `json:"tolerations,omitempty"`

// 	// Specifies the Environment variables.
// 	//+optional
// 	Resources *corev1.ResourceRequirements `json:"resources,omitempty"`
// }

// DeviceServerStatus defines the observed state of DeviceServer
type DeviceServerStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
	Replicas  int32  `json:"replicas"`
	Succeeded int32  `json:"succeeded"`
	State     string `json:"state"`
	Resources string `json:"resources"`
}

// DeviceServer is the Schema for the deviceservers API
// +kubebuilder:object:root=true
// +kubebuilder:subresource:status
// +kubebuilder:printcolumn:name="Components",type="integer",JSONPath=".status.replicas",description="The number of Components Requested in the DeviceServer",priority=0
// +kubebuilder:printcolumn:name="Succeeded",type="integer",JSONPath=".status.succeeded",description="The number of Components Launched in the DeviceServer",priority=0
// +kubebuilder:printcolumn:name="Age",type="date",JSONPath=".metadata.creationTimestamp",description="The number of Components Requested in the DeviceServer",priority=0
// +kubebuilder:printcolumn:name="State",type="string",JSONPath=".status.state",description="Status of the DeviceServer",priority=0
// +kubebuilder:printcolumn:name="Resources",type="string",JSONPath=".status.resources",description=" Resource details of the DeviceServer",priority=1
type DeviceServer struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   DeviceServerSpec   `json:"spec,omitempty"`
	Status DeviceServerStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// DeviceServerList contains a list of DeviceServer
type DeviceServerList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []DeviceServer `json:"items"`
}

func init() {
	SchemeBuilder.Register(&DeviceServer{}, &DeviceServerList{})
}
