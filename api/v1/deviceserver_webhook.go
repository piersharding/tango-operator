/*

Copyright 2022 Piers Harding
Copyright 2022 SKA Observatory

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
// +kubebuilder:docs-gen:collapse=Apache License

// Go imports
package v1

import (
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	validationutils "k8s.io/apimachinery/pkg/util/validation"
	"k8s.io/apimachinery/pkg/util/validation/field"

	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/webhook"
)

var deviceserverlog = ctrl.Log.WithName("deviceserver-resource")

func (r *DeviceServer) SetupWebhookWithManager(mgr ctrl.Manager) error {
	deviceserverlog.Info("Activating Webhook")
	return ctrl.NewWebhookManagedBy(mgr).
		For(r).
		Complete()
}

// +kubebuilder:webhook:path=/mutate-tango-tango-controls-org-v1-deviceserver,mutating=true,failurePolicy=fail,groups=tango.tango-controls.org,resources=deviceservers,verbs=create;update,versions=v1,name=mdeviceserver.tango-controls.org,admissionReviewVersions=v1,sideEffects=None

var _ webhook.Defaulter = &DeviceServer{}

// Default implements webhook.Defaulter so a webhook will be registered for the type
func (r *DeviceServer) Default() {
	deviceserverlog.Info("default", "name", r.Name)

	if r.Spec.Image == "" {
		r.Spec.Image = "tangocs/tango-pytango:latest"
	}

	if r.Spec.ImagePullPolicy == "" {
		r.Spec.ImagePullPolicy = "IfNotPresent"
	}

	if r.Spec.OrbPort == 0 {
		r.Spec.OrbPort = 45450
	}

	if r.Spec.ClusterDomain == "" {
		r.Spec.ClusterDomain = "cluster.local"
	}

	if r.Spec.HeartbeatPort == 0 {
		r.Spec.HeartbeatPort = 45460
	}

	if r.Spec.EventPort == 0 {
		r.Spec.EventPort = 45470
	}

	// if r.Spec.Daemon == nil {
	// 	r.Spec.Daemon = new(bool)
	// }

	// if r.Spec.Replicas == nil {
	// 	r.Spec.Replicas = new(int32)
	// 	*r.Spec.Replicas = 1
	// }

	// if r.Spec.Replicas == 0 {
	// 	r.Spec.Replicas = int32(5)
	// }
}

// TODO(user): change verbs to "verbs=create;update;delete" if you want to enable deletion validation.
// +kubebuilder:webhook:verbs=create;update,path=/validate-analytics-tango-controls-org-v1-deviceserver,mutating=false,failurePolicy=fail,groups=tango.tango-controls.org,resources=deviceservers,versions=v1,name=vdeviceserver.tango-controls.org,admissionReviewVersions=v1,sideEffects=None

var _ webhook.Validator = &DeviceServer{}

// ValidateCreate implements webhook.Validator so a webhook will be registered for the type
func (r *DeviceServer) ValidateCreate() error {
	deviceserverlog.Info("validate create", "name", r.Name)

	return r.validateDeviceServer()
}

// ValidateUpdate implements webhook.Validator so a webhook will be registered for the type
func (r *DeviceServer) ValidateUpdate(old runtime.Object) error {
	deviceserverlog.Info("validate update", "name", r.Name)

	return r.validateDeviceServer()
}

// ValidateDelete implements webhook.Validator so a webhook will be registered for the type
func (r *DeviceServer) ValidateDelete() error {
	deviceserverlog.Info("validate delete", "name", r.Name)

	// TODO(user): fill in your validation logic upon object deletion.
	return nil
}

func (r *DeviceServer) validateDeviceServer() error {
	var allErrs field.ErrorList
	if err := r.validateDeviceServerName(); err != nil {
		allErrs = append(allErrs, err)
	}
	if err := r.validateDeviceServerSpec(); err != nil {
		allErrs = append(allErrs, err)
	}
	if len(allErrs) == 0 {
		return nil
	}

	return apierrors.NewInvalid(
		schema.GroupKind{Group: "tango.tango-controls.org", Kind: "DeviceServer"},
		r.Name, allErrs)
}

func (r *DeviceServer) validateDeviceServerSpec() *field.Error {
	// The field helpers from the kubernetes API machinery help us return nicely
	// structured validation errors.
	// return validateScheduleFormat(
	// 	r.Spec.Schedule,
	// 	field.NewPath("spec").Child("schedule"))
	return nil
}

func (r *DeviceServer) validateDeviceServerName() *field.Error {
	if len(r.ObjectMeta.Name) > validationutils.DNS1035LabelMaxLength-11 {
		// The job name length is 63 character like all Kubernetes objects
		// (which must fit in a DNS subdomain). The cronjob controller appends
		// a 11-character suffix to the cronjob (`-$TIMESTAMP`) when creating
		// a job. The job name length limit is 63 characters. Therefore cronjob
		// names must have length <= 63-11=52. If we don't validate this here,
		// then job creation will fail later.
		return field.Invalid(field.NewPath("metadata").Child("name"), r.Name, "must be no more than 52 characters")
	}
	return nil
}
