/*

Copyright 2022 Piers Harding
Copyright 2022 SKA Observatory

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package models

import (
	"encoding/json"

	"github.com/appscode/go/log"
	dtypes "gitlab.com/piersharding/tango-operator/types"
	"gitlab.com/piersharding/tango-operator/utils"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
)

// DatabaseDSTangoDBService generates the Service description for
// the DatabaseDS
func DatabaseDSTangoDBService(dcontext dtypes.OperatorContext) (*corev1.Service, error) {

	const databaseDSTangoDBService = `
apiVersion: v1
kind: Service
metadata:
  name: databaseds-tangodb-{{ .Name }}
  namespace: {{ .Namespace }}
  labels:
    app.kubernetes.io/name: databaseds
    app.kubernetes.io/instance: "{{ .Name }}"
    app.kubernetes.io/managed-by: DatabaseDSController
spec:
  selector:
    app.kubernetes.io/name:  databaseds-tangodb
    app.kubernetes.io/instance: "{{ .Name }}"
  type: {{ .ServiceType }}
  ports:
  - name: mysql
    port: {{ .DBPort }}
    targetPort: mysql
    protocol: TCP
`
	result, err := utils.ApplyTemplate(databaseDSTangoDBService, dcontext)
	if err != nil {
		log.Debugf("ApplyTemplate Error: %+v\n", err)
		return nil, err
	}
	service := &corev1.Service{}
	if err := json.Unmarshal([]byte(result), service); err != nil {
		return nil, err
	}
	return service, err
}

// DatabaseDSTangoDBStorage generates the storage volume for the TangoDB
func DatabaseDSTangoDBStorage(dcontext dtypes.OperatorContext) (*corev1.PersistentVolumeClaim, error) {

	const databaseDSTangoDBStorage = `
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: databaseds-tangodb-pvc-{{ .Name }}
  namespace: {{ .Namespace }}
  labels:
    app.kubernetes.io/name: databaseds-tangodb-pvc
    app.kubernetes.io/instance: "{{ .Name }}"
    app.kubernetes.io/managed-by: DatabaseDSController
spec:
  accessModes:
    - ReadWriteMany
  storageClassName: {{ .TangoDBStorageClass }}
  resources:
    requests:
      storage: 1Gi
`

	result, err := utils.ApplyTemplate(databaseDSTangoDBStorage, dcontext)
	if err != nil {
		log.Debugf("ApplyTemplate Error: %+v\n", err)
		return nil, err
	}

	pvc := &corev1.PersistentVolumeClaim{}
	if err := json.Unmarshal([]byte(result), pvc); err != nil {
		return nil, err
	}
	return pvc, err
}

// DatabaseDSTangoDBStatefulSet generates the StatefulSet description for
// the DatabaseDS
func DatabaseDSTangoDBStatefulSet(dcontext dtypes.OperatorContext) (*appsv1.StatefulSet, error) {

	const databaseDSTangoDBStatefulSet = `
---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: databaseds-tangodb-{{ .Name }}
  namespace: {{ .Namespace }}
  labels:
    app.kubernetes.io/name: databaseds-tangodb
    app.kubernetes.io/instance: "{{ .Name }}"
    app.kubernetes.io/managed-by: DatabaseDSController
  annotations:
    checksum/config: {{ .ConfigCheckSum }}
spec:
  selector:
    matchLabels:
      app.kubernetes.io/name: databaseds-tangodb
      app.kubernetes.io/instance: "{{ .Name }}"
  replicas: 1
  template:
    metadata:
      labels:
        app.kubernetes.io/name: databaseds-tangodb
        app.kubernetes.io/instance: "{{ .Name }}"
        app.kubernetes.io/managed-by: DatabaseDSController
    spec:
      serviceAccountName: "databaseds-serviceaccount-{{ .Name }}"
    {{- with .PullSecrets }}
      imagePullSecrets:
      {{range $val := .}}
      - name: {{ $val.name }}
      {{end}}
      {{- end }}
      containers:
      - name: tangodb
        image: "{{ .DBImage }}"
        imagePullPolicy: {{ .PullPolicy }}
        command:
          - /start-databaseds-tangodb.sh
        env:
          - name: MYSQL_ROOT_PASSWORD
            value: "{{ .DBRootPw }}"
          - name: MYSQL_DATABASE
            value: "{{ .DBDb }}"
          - name: MYSQL_USER
            value: "{{ .DBUser }}"
          - name: MYSQL_PASSWORD
            value: "{{ .DBPassword }}"
          - name: MYSQL_ALLOW_EMPTY_PASSWORD
            value: "1"
          - name: MYSQL_INITDB_SKIP_TZINFO
            value: "1"
{{- with .Env }}
{{ toYaml . | indent 10 }}
{{- end }}
        ports:
        - name: mysql
          containerPort: {{ .DBPort }}
        volumeMounts:
        - mountPath: /start-databaseds-tangodb.sh
          subPath: start-databaseds-tangodb.sh
          name: databaseds-script
        #- mountPath: /var/tmp
        #  readOnly: false
        #  name: localdir
        - name: data
          mountPath: /var/lib/mysql
{{- with .VolumeMounts }}
{{ toYaml . | indent 8 }}
{{- end }}
        livenessProbe:
          exec:
            command: ["mysqladmin", "--user", "root", "--password={{ .DBRootPw }}", "ping"]
          initialDelaySeconds: 30
          periodSeconds: 10
          timeoutSeconds: 5
        readinessProbe:
          exec:
            # Check we can execute queries over TCP (skip-networking is off).
            command: ["mysql", "-u", "root", "-p{{ .DBRootPw }}", "-e", "SELECT 1"]
          initialDelaySeconds: 5
          periodSeconds: 2
          timeoutSeconds: 1
      volumes:
      - configMap:
          name: databaseds-configs-{{ .Name }}
          defaultMode: 0777
        name: databaseds-script
      - name: data
        persistentVolumeClaim:
          claimName: databaseds-tangodb-pvc-{{ .Name }}
      #- hostPath:
      #    path: /var/tmp
      #    type: DirectoryOrCreate
      #  name: localdir
      #- name: localdir
      #  emptyDir: {}
{{- with .Volumes }}
{{ toYaml . | indent 6 }}
{{- end }}
{{- with .NodeSelector }}
      nodeSelector:
{{ toYaml . | indent 8 }}
{{- end }}
{{- with .Affinity }}
      affinity:
{{ toYaml . | indent 8 }}
{{- end }}
{{- with .Tolerations }}
      tolerations:
{{ toYaml . | indent 8 }}
{{- end }}

`

	result, err := utils.ApplyTemplate(databaseDSTangoDBStatefulSet, dcontext)
	if err != nil {
		log.Debugf("ApplyTemplate Error: %+v\n", err)
		return nil, err
	}

	statefulset := &appsv1.StatefulSet{}
	if err := json.Unmarshal([]byte(result), statefulset); err != nil {
		return nil, err
	}
	return statefulset, err
}

// DatabaseDSService generates the Service description for
// the DatabaseDS
func DatabaseDSService(dcontext dtypes.OperatorContext) (*corev1.Service, error) {

	const databaseDSService = `
apiVersion: v1
kind: Service
metadata:
  name: databaseds-{{ .Name }}
  namespace: {{ .Namespace }}
  labels:
    app.kubernetes.io/name: databaseds
    app.kubernetes.io/instance: "{{ .Name }}"
    app.kubernetes.io/managed-by: DatabaseDSController
spec:
  selector:
    app.kubernetes.io/name:  databaseds-ds
    app.kubernetes.io/instance: "{{ .Name }}"
  type: {{ .ServiceType }}
  ports:
  - name: ds
    port: {{ .Port }}
    targetPort: ds
    protocol: TCP
`
	result, err := utils.ApplyTemplate(databaseDSService, dcontext)
	if err != nil {
		log.Debugf("ApplyTemplate Error: %+v\n", err)
		return nil, err
	}
	service := &corev1.Service{}
	if err := json.Unmarshal([]byte(result), service); err != nil {
		return nil, err
	}
	return service, err
}

// DatabaseDSStatefulSet generates the StatefulSet description for
// the DatabaseDS
func DatabaseDSStatefulSet(dcontext dtypes.OperatorContext) (*appsv1.StatefulSet, error) {

	const databaseDSStatefulSet = `
---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: databaseds-ds-{{ .Name }}
  namespace: {{ .Namespace }}
  labels:
    app.kubernetes.io/name: databaseds-ds
    app.kubernetes.io/instance: "{{ .Name }}"
    app.kubernetes.io/managed-by: DatabaseDSController
  annotations:
    checksum/config: {{ .ConfigCheckSum }}
spec:
  selector:
    matchLabels:
      app.kubernetes.io/name: databaseds-ds
      app.kubernetes.io/instance: "{{ .Name }}"
  replicas: 1
  template:
    metadata:
      labels:
        app.kubernetes.io/name: databaseds-ds
        app.kubernetes.io/instance: "{{ .Name }}"
        app.kubernetes.io/managed-by: DatabaseDSController
    spec:
      serviceAccountName: "databaseds-serviceaccount-{{ .Name }}"
    {{- with .PullSecrets }}
      imagePullSecrets:
      {{range $val := .}}
      - name: {{ $val.name }}
      {{end}}
      {{- end }}
      containers:
      - name: ds
        image: "{{ .DDSImage }}"
        imagePullPolicy: {{ .PullPolicy }}
        command:
          - /start-databaseds.sh
        env:
          - name: MYSQL_HOST
            value: "databaseds-tangodb-{{ .Name }}:3306"
          - name: MYSQL_DATABASE
            value: "{{ .DBDb }}"
          - name: MYSQL_USER
            value: "{{ .DBUser }}"
          - name: MYSQL_PASSWORD
            value: "{{ .DBPassword }}"
{{- with .Env }}
{{ toYaml . | indent 10 }}
{{- end }}
        ports:
        - name: ds
          containerPort: {{ .Port }}
        volumeMounts:
        - mountPath: /start-databaseds.sh
          subPath: start-databaseds.sh
          name: databaseds-script
{{- with .VolumeMounts }}
{{ toYaml . | indent 8 }}
{{- end }}
        readinessProbe:
          tcpSocket:
            port: {{ .Port }}
          initialDelaySeconds: 10
          periodSeconds: 10
          timeoutSeconds: 3
          failureThreshold: 6
        livenessProbe:
          tcpSocket:
            port: {{ .Port }}
          initialDelaySeconds: 3
          periodSeconds: 10
          timeoutSeconds: 3
          failureThreshold: 6
      volumes:
      - configMap:
          name: databaseds-configs-{{ .Name }}
          defaultMode: 0777
        name: databaseds-script
{{- with .Volumes }}
{{ toYaml . | indent 6 }}
{{- end }}
{{- with .NodeSelector }}
      nodeSelector:
{{ toYaml . | indent 8 }}
{{- end }}
{{- with .Affinity }}
      affinity:
{{ toYaml . | indent 8 }}
{{- end }}
{{- with .Tolerations }}
      tolerations:
{{ toYaml . | indent 8 }}
{{- end }}

`

	result, err := utils.ApplyTemplate(databaseDSStatefulSet, dcontext)
	if err != nil {
		log.Debugf("ApplyTemplate Error: %+v\n", err)
		return nil, err
	}

	statefulset := &appsv1.StatefulSet{}
	if err := json.Unmarshal([]byte(result), statefulset); err != nil {
		return nil, err
	}
	return statefulset, err
}
