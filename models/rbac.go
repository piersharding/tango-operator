/*

Copyright 2022 Piers Harding
Copyright 2022 SKA Observatory

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package models

import (
	"encoding/json"

	"github.com/appscode/go/log"
	dtypes "gitlab.com/piersharding/tango-operator/types"
	"gitlab.com/piersharding/tango-operator/utils"
	corev1 "k8s.io/api/core/v1"
)

// DatabaseDSServiceAccount generates the ServiceAccount description for
// for all resources
func DatabaseDSServiceAccount(dcontext dtypes.OperatorContext) (*corev1.ServiceAccount, error) {
	const serviceAccount = `
apiVersion: v1
kind: ServiceAccount
metadata:
  name: databaseds-serviceaccount-{{ .Name }}
  namespace: {{ .Namespace }}
  labels:
    app.kubernetes.io/name: databaseds-serviceaccount
    app.kubernetes.io/instance: "{{ .Name }}"
    app.kubernetes.io/managed-by: DatabaseDSController
`

	result, err := utils.ApplyTemplate(serviceAccount, dcontext)
	if err != nil {
		log.Debugf("ApplyTemplate Error: %+v\n", err)
		return nil, err
	}

	serviceaccount := &corev1.ServiceAccount{}
	if err := json.Unmarshal([]byte(result), serviceaccount); err != nil {
		return nil, err
	}
	return serviceaccount, err
}

// DeviceServerServiceAccount generates the ServiceAccount description for
// for all resources
func DeviceServerServiceAccount(dcontext dtypes.OperatorContext) (*corev1.ServiceAccount, error) {
	const serviceAccount = `
apiVersion: v1
kind: ServiceAccount
metadata:
  name: deviceserver-serviceaccount-{{ .Name }}
  namespace: {{ .Namespace }}
  labels:
    app.kubernetes.io/name: deviceserver-serviceaccount
    app.kubernetes.io/instance: "{{ .Name }}"
    app.kubernetes.io/managed-by: DeviceServerController
`

	result, err := utils.ApplyTemplate(serviceAccount, dcontext)
	if err != nil {
		log.Debugf("ApplyTemplate Error: %+v\n", err)
		return nil, err
	}

	serviceaccount := &corev1.ServiceAccount{}
	if err := json.Unmarshal([]byte(result), serviceaccount); err != nil {
		return nil, err
	}
	return serviceaccount, err
}
