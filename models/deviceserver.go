/*

Copyright 2022 Piers Harding
Copyright 2022 SKA Observatory

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package models

import (
	"encoding/json"

	"github.com/appscode/go/log"
	dtypes "gitlab.com/piersharding/tango-operator/types"
	"gitlab.com/piersharding/tango-operator/utils"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
)

// DeviceServerService generates the Service description for
// the DeviceServer
func DeviceServerService(dcontext dtypes.OperatorContext) (*corev1.Service, error) {

	const deviceServerService = `
apiVersion: v1
kind: Service
metadata:
  name: deviceserver-{{ .Name }}
  namespace: {{ .Namespace }}
  labels:
    app.kubernetes.io/name: deviceserver
    app.kubernetes.io/instance: "{{ .Name }}"
    app.kubernetes.io/managed-by: DeviceServerController
spec:
  selector:
    app.kubernetes.io/name:  deviceserver
    app.kubernetes.io/instance: "{{ .Name }}"
  type: {{ .ServiceType }}
  {{ if .ExternalDeviceServer }}
  externalName: {{ .ExternalDeviceServer }}
  {{ else }}
  ports:
  - name: tango-server
    port: {{ .OrbPort }}
    targetPort: tango-server
  - name: tango-heartbeat
    port: {{ .HeartbeatPort }}
    targetPort: tango-heartbeat
  - name: tango-event
    port: {{ .EventPort }}
    targetPort: tango-event
  {{ end }}
`
	result, err := utils.ApplyTemplate(deviceServerService, dcontext)
	if err != nil {
		log.Debugf("ApplyTemplate Error: %+v\n", err)
		return nil, err
	}
	service := &corev1.Service{}
	if err := json.Unmarshal([]byte(result), service); err != nil {
		return nil, err
	}
	return service, err
}

// DeviceServerStatefulSet generates the StatefulSet description for
// the DeviceServer
func DeviceServerStatefulSet(dcontext dtypes.OperatorContext) (*appsv1.StatefulSet, error) {

	const deviceServerStatefulSet = `
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: deviceserver-{{ .Name }}
  namespace: {{ .Namespace }}
  labels:
    app.kubernetes.io/name: deviceserver
    app.kubernetes.io/instance: "{{ .Name }}"
    app.kubernetes.io/managed-by: DeviceServerController
  annotations:
    checksum/config: {{ .ConfigCheckSum }}
spec:
  selector:
    matchLabels:
      app.kubernetes.io/name: deviceserver
      app.kubernetes.io/instance: "{{ .Name }}"
  replicas: 1
  template:
    metadata:
      labels:
        app.kubernetes.io/name: deviceserver
        app.kubernetes.io/instance: "{{ .Name }}"
        app.kubernetes.io/managed-by: DeviceServerController
    spec:
      serviceAccountName: "deviceserver-serviceaccount-{{ .Name }}"
    {{- with .PullSecrets }}
      imagePullSecrets:
      {{range $val := .}}
      - name: {{ $val.name }}
      {{end}}
    {{- end }}

    {{ if or (gt (len .ConfigContents) 0) (gt (len .DependsOn) 0) }}

      initContainers:
      {{- if gt (len .DependsOn) 0 }}
      - name: dependson
        image: "{{ .DSConfigImage }}"
        imagePullPolicy: "{{ .PullPolicy }}"
        command:
        - sh
        - /dependson.sh
        env:
        - name: TANGO_HOST
          value: {{ .DatabaseDS }}:{{ .Port }}
        volumeMounts:
        - mountPath: /dependson.sh
          subPath: dependson.sh
          name: deviceserver-configuration
        resources: {}
        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: FallbackToLogsOnError
      {{- end }}
      {{ if gt (len .ConfigContents) 0 }}
      - name: configurator
        image: "{{ .DSConfigImage }}"
        imagePullPolicy: "{{ .PullPolicy }}"
        command:
        - sh
        - /dsconfig.sh
        env:
        - name: TANGO_HOST
          value: {{ .DatabaseDS }}:{{ .Port }}
        {{- with .Env }}
        {{ toYaml . | indent 10 }}
        {{- end }}
        volumeMounts:
        - mountPath: /configuration.json
          subPath: configuration.json
          name: deviceserver-configuration
        - mountPath: /dsconfig.sh
          subPath: dsconfig.sh
          name: deviceserver-configuration
        resources: {}
        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: FallbackToLogsOnError
      {{- end }}
      {{- end }}
      containers:
      - name: deviceserver
        image: "{{ .Image }}"
        imagePullPolicy: "{{ .PullPolicy }}"
        command:
          - /start-deviceserver.sh
        ports:
        - containerPort: {{ .OrbPort }}
          name: tango-server
        - containerPort: {{ .HeartbeatPort }}
          name: tango-heartbeat
        - containerPort: {{ .EventPort }}
          name: tango-event
        env:
        - name: TANGO_HOST
          value: {{ .DatabaseDS }}.{{ .Namespace }}.svc.{{ .ClusterDomain }}:{{ .Port }}
        - name: TANGO_ZMQ_EVENT_PORT
          value: "{{ .EventPort }}"
        - name: TANGO_ZMQ_HEARTBEAT_PORT
          value: "{{ .HeartbeatPort }}"
                {{- with .Env }}
{{ toYaml . | indent 10 }}
{{- end }}
        volumeMounts:
        - mountPath: /start-deviceserver.sh
          subPath: start-deviceserver.sh
          name: deviceserver-configuration
        - mountPath: /{{ .AppName }}
          subPath: {{ .AppName }}
          name: deviceserver-configuration
{{- with .VolumeMounts }}
{{ toYaml . | indent 8 }}
{{- end }}
        readinessProbe:
          tcpSocket:
            port: {{ .OrbPort }}
          initialDelaySeconds: 1
          periodSeconds: 3
          timeoutSeconds: 3
          failureThreshold: 20
        livenessProbe:
          tcpSocket:
            port: {{ .OrbPort }}
          initialDelaySeconds: 3
          periodSeconds: 10
          timeoutSeconds: 3
          failureThreshold: 6
      volumes:
      - name: deviceserver-configuration
        configMap:
          defaultMode: 0777
          name: "deviceserver-configs-{{ .Name }}"
{{- with .Volumes }}
{{ toYaml . | indent 6 }}
{{- end }}
{{- with .NodeSelector }}
      nodeSelector:
{{ toYaml . | indent 8 }}
{{- end }}
{{- with .Affinity }}
      affinity:
{{ toYaml . | indent 8 }}
{{- end }}
{{- with .Tolerations }}
      tolerations:
{{ toYaml . | indent 8 }}
{{- end }}

`

	result, err := utils.ApplyTemplate(deviceServerStatefulSet, dcontext)
	if err != nil {
		log.Debugf("ApplyTemplate Error: %+v\n", err)
		return nil, err
	}

	deployment := &appsv1.StatefulSet{}
	if err := json.Unmarshal([]byte(result), deployment); err != nil {
		return nil, err
	}
	return deployment, err
}
