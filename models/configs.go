/*

Copyright 2022 Piers Harding
Copyright 2022 SKA Observatory

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package models

import (
	"encoding/json"

	"github.com/appscode/go/log"
	dtypes "gitlab.com/piersharding/tango-operator/types"
	"gitlab.com/piersharding/tango-operator/utils"
	corev1 "k8s.io/api/core/v1"
)

// DeviceServerConfigs generates the ConfigMap for
// the DeviceServer
func DeviceServerConfigs(dcontext dtypes.OperatorContext) (*corev1.ConfigMap, error) {

	const deviceserverConfigs = `
# Device Server configurations
apiVersion: v1
kind: ConfigMap
metadata:
    name: deviceserver-configs-{{ .Name }}
    namespace: {{ .Namespace }}
    labels:
      app.kubernetes.io/name: deviceserver-configs
      app.kubernetes.io/instance: "{{ .Name }}"
      app.kubernetes.io/managed-by: DeviceServerController
data:

  start-deviceserver.sh: |
    #!/usr/bin/env bash
    set -o errexit -o pipefail

    [ -f "${HOME}/.bash_profile" ] && source "${HOME}/.bash_profile"
    export TANGO_ZMQ_EVENT_PORT={{ .EventPort }} TANGO_ZMQ_HEARTBEAT_PORT={{ .HeartbeatPort }}

    echo "Complete environment:"
    printenv

    # is this a script or an App
    {{ if .ScriptContents }}
    exec python /{{ .AppName }} {{ .Args }} -ORBendPoint giop:tcp::{{ .OrbPort }} -ORBendPointPublish giop:tcp:deviceserver-{{ .Name }}.{{ .Namespace }}.svc.{{ .ClusterDomain }}:{{ .OrbPort }}
    {{ else }}
    exec python /{{ .Script }} {{ .Args }} -ORBendPoint giop:tcp::{{ .OrbPort }} -ORBendPointPublish giop:tcp:deviceserver-{{ .Name }}.{{ .Namespace }}.svc.{{ .ClusterDomain }}:{{ .OrbPort }}
    {{ end }}

  dependson.sh: |
    #!/bin/sh
    set -x
    echo "Complete environment for depends check:"
    printenv
    ls -latr /

    {{ if .DependsOn }}
    {{- with .DependsOn }}
    {{range $val := .}}
    /usr/local/bin/tango_admin --ping-device {{ $val }}
    rc=$?
    if [ $rc -eq 0 ]; then
      echo "Ping of {{ $val }} finished normally."
    else
      echo "Ping of {{ $val }} finished with an ERROR [$rc]."
      exit 1
    fi
    {{end}}
    {{- end }}
    {{- end }}

    echo "finished normally."
    exit 0

  dsconfig.sh: |
    #!/bin/sh
    [ -f "${HOME}/.bash_profile" ] && source "${HOME}/.bash_profile"

    echo "Complete environment for ds config load:"
    printenv
    set -x
    ls -latr /

    json2tango -w -a -u /configuration.json
    rc=$?
    if [ $rc -eq 0 ]; then
      echo "finished normally."
      exit 0
    else
      if [ $rc -eq 2 ]; then
        echo "finished with an update."
        exit 0
      else
        echo "finished with an ERROR."
        exit $rc
      fi
    fi

  configuration.json: |
{{ .ConfigContents | indent 4 }}

  {{ .AppName }}: |
{{ .ScriptContents | indent 4 }}


`
	result, err := utils.ApplyTemplate(deviceserverConfigs, dcontext)
	if err != nil {
		log.Debugf("ApplyTemplate Error: %+v\n", err)
		return nil, err
	}
	configmap := &corev1.ConfigMap{}
	if err := json.Unmarshal([]byte(result), configmap); err != nil {
		return nil, err
	}
	return configmap, err
}

// DatabaseDSConfigs generates the ConfigMap for
// the DatabaseDS
func DatabaseDSConfigs(dcontext dtypes.OperatorContext) (*corev1.ConfigMap, error) {

	const databaseDSConfigs = `
apiVersion: v1
kind: ConfigMap
metadata:
  name: databaseds-configs-{{ .Name }}
  namespace: {{ .Namespace }}
  labels:
    app.kubernetes.io/name: databaseds-configs
    app.kubernetes.io/instance: "{{ .Name }}"
    app.kubernetes.io/managed-by: DatabaseDSController
data:

  start-databaseds-tangodb.sh: |
    #!/usr/bin/env bash
    set -o errexit -o pipefail

    [ -f "${HOME}/.bash_profile" ] && source "${HOME}/.bash_profile"

    echo "Complete environment:"
    printenv

    ls -latr /docker-entrypoint-initdb.d/*

    # exec /scripts/run.sh
    exec docker-entrypoint.sh mariadbd

  start-databaseds.sh: |
    #!/usr/bin/env bash
    set -o errexit -o pipefail

    [ -f "${HOME}/.bash_profile" ] && source "${HOME}/.bash_profile"

    echo "Complete environment:"
    printenv

    exec /usr/local/bin/DataBaseds 2 -ORBendPoint giop:tcp::{{ .Port }} -ORBendPointPublish giop:tcp:{{ .DatabaseDS }}.{{ .Namespace }}.svc.{{ .ClusterDomain }}:{{ .Port }}


  config.json: |
{{ .ScriptContents | indent 4 }}

`
	result, err := utils.ApplyTemplate(databaseDSConfigs, dcontext)
	if err != nil {
		log.Debugf("ApplyTemplate Error: %+v\n", err)
		return nil, err
	}
	configmap := &corev1.ConfigMap{}
	if err := json.Unmarshal([]byte(result), configmap); err != nil {
		return nil, err
	}
	return configmap, err
}
