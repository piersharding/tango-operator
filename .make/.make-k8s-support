#!/usr/bin/env bash

# Shellscript support function file for Kubernetes Make targets

function k8sChartVersion() {
	if [ -z "$1" ] ; then
		echo "k8sChartVersion: Missing K8S_CHART"
        exit 1
	fi
    K8S_CHART="$1"
    if ! [[ $K8S_CHART == ska* ]]; then
		echo "k8sChartVersion: Missing K8S_CHART (${K8S_CHART}) must start with ska-"
        exit 1
    fi
    if [[ -z "${K8S_HELM_REPOSITORY}" ]]; then
		echo "k8sChartVersion: Missing K8S_HELM_REPOSITORY"
        exit 1
    fi
    (helm repo list | grep "${K8S_HELM_REPOSITORY}" >/dev/null) || (echo "k8sChartVersion: repo ${K8S_HELM_REPOSITORY} is not set"; exit 1;)
    K8S_REPO=$(helm repo list | grep "${K8S_HELM_REPOSITORY}" | awk '{print $1}')
    helm repo update ${K8S_REPO} >/dev/null 2>&1
    k8s_ver_check=$?
    if ! [[ $k8s_ver_check -eq 0 ]]; then
		echo "k8sChartVersion: could not update K8S_HELM_REPOSITORY ${K8S_HELM_REPOSITORY}(${K8S_REPO})"
        exit 1
    fi
    which jq >/dev/null 2>&1 || (echo "jq not installed - see https://stedolan.github.io/jq/"; exit 1;)
    K8S_CHART_VERSION=`helm search repo -o json ${K8S_CHART}  | jq -r '.[].version' | head -1`;
    if [[ -z "${K8S_CHART_VERSION}" ]]; then
		echo "k8sChartVersion: version for ${K8S_CHART} in ${K8S_HELM_REPOSITORY} could not be found"
        exit 1
    fi
    echo ${K8S_CHART_VERSION}
}

function k8sWait() {
	if [ -z "$1" ] ; then
		echo "k8sWait: Missing KUBE_NAMESPACE"
        exit 1
	fi
    KUBE_NAMESPACE="$1"
	echo "k8sWait: waiting for pods to be ready in ${KUBE_NAMESPACE}"
	date
	kubectl -n ${KUBE_NAMESPACE} get pods
	date
	k8s_wait_jobs=$(kubectl get job --output=jsonpath={.items..metadata.name} -n ${KUBE_NAMESPACE})
	echo "k8sWait: Jobs found: $k8s_wait_jobs"
	if [[ -z "${k8s_wait_jobs}" ]]; then
		echo "k8sWait: no Jobs found to wait for using: kubectl get job --output=jsonpath={.items..metadata.name} -n ${KUBE_NAMESPACE}"
	else
		time kubectl wait job --for=condition=complete --timeout=${K8S_TIMEOUT} $k8s_wait_jobs -n ${KUBE_NAMESPACE}
		wait_result=$?
		if [[ $wait_result == 0 ]]; then
			echo "k8sWait: Jobs complete - $k8s_wait_jobs "
		else
			echo "k8sWait: jobs FAILED! "
			kubectl get events -n ${KUBE_NAMESPACE} --sort-by=.metadata.creationTimestamp | tac
			kubectl -n ${KUBE_NAMESPACE} get job
			k8sPodLogs ${KUBE_NAMESPACE} ${KUBE_APP}
			exit $wait_result
		fi
	fi
	date
	k8s_wait_pods=$(kubectl get pod -l app=${KUBE_APP} --output=jsonpath={.items..metadata.name} -n ${KUBE_NAMESPACE})
	echo "k8sWait: Pods found: $k8s_wait_pods"
	if [[ -z "${k8s_wait_pods}" ]]; then
		echo "k8sWait: no Pods found to wait for using: kubectl get pod -l app=${KUBE_APP} --output=jsonpath={.items..metadata.name} -n ${KUBE_NAMESPACE}"
		exit 0
	fi
	echo "k8sWait: going to - kubectl -n ${KUBE_NAMESPACE} wait --for=condition=ready --timeout=${K8S_TIMEOUT} pods ${k8s_wait_pods}"
	time kubectl -n ${KUBE_NAMESPACE} wait --for=condition=ready --timeout=${K8S_TIMEOUT} pods ${k8s_wait_pods}
	wait_result=$?
	if [[ $wait_result == 0 ]]; then
		echo "k8sWait: all Pods ready "
		kubectl -n ${KUBE_NAMESPACE} get pods -o jsonpath='{range .items[*]}Pod: {@.metadata.name}{"\n"}Containers:{"\n"}{range @.spec.containers[*]}{@.name} => {@.image}{"\n"}{end}{"\n"}{end}'
	else
		echo "k8sWait: Pods FAILED! "
		kubectl get events -n ${KUBE_NAMESPACE} --sort-by=.metadata.creationTimestamp | tac
		kubectl -n ${KUBE_NAMESPACE} get pods
		k8sPodLogs ${KUBE_NAMESPACE} ${KUBE_APP}
		exit $wait_result
	fi;
	date
}

function k8sDescribe() {
	if [ -z "$1" ] ; then
		echo "k8sDescribe: Missing KUBE_NAMESPACE"
        exit 1
	fi
    KUBE_NAMESPACE="$1"
	if [ -z "$2" ] ; then
		echo "k8sDescribe: Missing KUBE_APP"
        exit 1
	fi
    KUBE_APP="$2"

	for i in `kubectl -n ${KUBE_NAMESPACE} get pods -l app=${KUBE_APP} -o=name`
	do
        echo "---------------------------------------------------"
        echo "k8sDescribe: describe for ${i}"
        echo kubectl -n ${KUBE_NAMESPACE} describe ${i}
        echo "---------------------------------------------------"
        kubectl -n ${KUBE_NAMESPACE} describe ${i}
        echo "---------------------------------------------------"
        echo ""; echo ""; echo ""
	done
}

function k8sPodLogs() {
	if [ -z "$1" ] ; then
		echo "k8sPodLogs: Missing KUBE_NAMESPACE"
        exit 1
	fi
    KUBE_NAMESPACE="$1"
	if [ -z "$2" ] ; then
		echo "k8sPodLogs: Missing KUBE_APP"
        exit 1
	fi
    KUBE_APP="$2"

	for i in `kubectl -n ${KUBE_NAMESPACE} get pods -l app=${KUBE_APP} -o=name`
	do \
        echo "---------------------------------------------------"
        echo "Logs for ${i}"
        echo kubectl -n ${KUBE_NAMESPACE} logs ${i}
        echo kubectl -n ${KUBE_NAMESPACE} get ${i} -o jsonpath="{.spec.initContainers[*].name}"
        echo "---------------------------------------------------"
        for j in `kubectl -n ${KUBE_NAMESPACE} get ${i} -o jsonpath="{.spec.initContainers[*].name}"`; do \
            RES=`kubectl -n ${KUBE_NAMESPACE} logs ${i} -c ${j} 2>/dev/null`
            echo "initContainer: ${j}"; echo "${RES}"
            echo "---------------------------------------------------";\
        done
        echo "Main Pod logs for ${i}"
        echo "---------------------------------------------------"
        for j in `kubectl -n ${KUBE_NAMESPACE} get ${i} -o jsonpath="{.spec.containers[*].name}"`; do \
            RES=`kubectl -n ${KUBE_NAMESPACE} logs ${i} -c ${j} 2>/dev/null`
            echo "Container: ${j}"; echo "${RES}"
            echo "---------------------------------------------------";\
        done
        echo "---------------------------------------------------"
        echo ""; echo ""; echo ""
	done
}
