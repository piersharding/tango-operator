/*

Copyright 2022 Piers Harding
Copyright 2022 SKA Observatory

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package types

import (
	"fmt"

	"github.com/appscode/go/log"
	"github.com/go-logr/logr"
	tangov1 "gitlab.com/piersharding/tango-operator/api/v1"
	appsv1 "k8s.io/api/apps/v1"
)

// Image Default Container Image for DeviceServer
var Image string

// DDSImage Default Container Image for DatabaseDS
var DDSImage string

// DBImage Default Container Image for TangoDB
var DBImage string

// dsconfig command image
var DSConfigImage string

// PullPolicy Default image pull policy
var PullPolicy string

// TangoAdmin executable path
var TangoAdmin string

// Json2Tango executable path
var Json2Tango string

// OperatorContext is the set of parameters to configures this instance
type OperatorContext struct {
	// Daemon              bool
	CurrentTangoDBStatefulSet *appsv1.StatefulSet
	ConfigCheckSum            string
	DisablePolicies           bool
	ExternalDeviceServer      string
	EnableLoadBalancer        bool
	Namespace                 string
	Name                      string
	ServiceType               string
	ClusterDomain             string
	Port                      int32
	DBPort                    int32
	OrbPort                   int32
	HeartbeatPort             int32
	EventPort                 int32
	Replicas                  int32
	DatabaseDS                string
	DirectConnection          bool
	Script                    string
	ScriptType                string
	ScriptContents            string
	Config                    string
	AppName                   string
	Args                      string
	ConfigType                string
	ConfigContents            string
	MountedFile               bool
	Image                     string
	DDSImage                  string
	DBImage                   string
	DSConfigImage             string
	DBDb                      string
	DBRootPw                  string
	DBUser                    string
	DBPassword                string
	TangoDBStorageClass       string
	Repository                string
	Tag                       string
	DependsOn                 interface{}
	PullSecrets               interface{}
	PullPolicy                string
	NodeSelector              interface{}
	Affinity                  interface{}
	Tolerations               interface{}
	Resources                 interface{}
	VolumeMounts              interface{}
	Volumes                   interface{}
	Env                       interface{}
}

// SetConfig setup the configuration
func SetConfig(deviceserver tangov1.DeviceServer) OperatorContext {

	context := OperatorContext{
		CurrentTangoDBStatefulSet: nil,
		DisablePolicies:           deviceserver.Spec.DisablePolicies,
		Namespace:                 deviceserver.Namespace,
		Name:                      deviceserver.Name,
		ServiceType:               "ClusterIP",
		ClusterDomain:             "cluster.local",
		Port:                      10000,
		DBPort:                    3306,
		OrbPort:                   deviceserver.Spec.OrbPort,
		HeartbeatPort:             deviceserver.Spec.HeartbeatPort,
		EventPort:                 deviceserver.Spec.EventPort,
		Image:                     deviceserver.Spec.Image,
		DDSImage:                  "",
		DBImage:                   "",
		DSConfigImage:             DSConfigImage,
		DBDb:                      "",
		DBRootPw:                  "",
		DBUser:                    "",
		DBPassword:                "",
		TangoDBStorageClass:       "",
		DatabaseDS:                deviceserver.Spec.DatabaseDS,
		ExternalDeviceServer:      deviceserver.Spec.ExternalDeviceServer,
		DirectConnection:          deviceserver.Spec.DirectConnection,
		EnableLoadBalancer:        deviceserver.Spec.EnableLoadBalancer,
		Script:                    deviceserver.Spec.Script,
		ScriptContents:            "",
		Config:                    deviceserver.Spec.Config,
		AppName:                   "app.py",
		Args:                      deviceserver.Spec.Args,
		DependsOn:                 deviceserver.Spec.DependsOn,
		ConfigContents:            "",
		MountedFile:               false,
		PullSecrets:               deviceserver.Spec.PullSecrets,
		PullPolicy:                deviceserver.Spec.ImagePullPolicy,
		NodeSelector:              deviceserver.Spec.NodeSelector,
		Affinity:                  deviceserver.Spec.Affinity,
		Tolerations:               deviceserver.Spec.Tolerations,
		Resources:                 deviceserver.Spec.Resources,
		VolumeMounts:              deviceserver.Spec.VolumeMounts,
		Volumes:                   deviceserver.Spec.Volumes,
		Env:                       deviceserver.Spec.Env}

	log.Debugf("context: %+v", context)
	return context
}

// ForDatabaseDS - copy and arrange config values for Scheduler
func (context *OperatorContext) ForDatabaseDS() OperatorContext {
	out := new(OperatorContext)
	out = context

	// if context != nil {
	// 	if context.DBImage == "" {
	// 		out.DBImage = DBImage
	// 	}
	// 	if context.PullPolicy == "" {
	// 		out.PullPolicy = PullPolicy
	// 	}
	// }

	// out.applySpecifics(context.Scheduler.(*tangov1.DeviceServerDeploymentSpec))
	return *out
}

// CustomLogger - add Errorf, Infof, and Debugf
type CustomLogger struct {
	logr.Logger
}

// WithValues helper
func (log *CustomLogger) WithValues(keysAndValues ...interface{}) CustomLogger {
	return CustomLogger{Logger: log.Logger.WithValues(keysAndValues...)}
}

// Errorf helper
func (log *CustomLogger) Errorf(err error, format string, a ...interface{}) {
	log.Logger.Error(err, fmt.Sprintf(format, a...))
}

// Infof helper
func (log CustomLogger) Infof(format string, a ...interface{}) {
	log.Logger.Info(fmt.Sprintf(format, a...))
}

// Debugf helper
func (log CustomLogger) Debugf(format string, a ...interface{}) {
	log.Logger.Info(fmt.Sprintf(format, a...))
}

// DSSetConfig setup the configuration
func DSSetConfig(databaseds tangov1.DatabaseDS) OperatorContext {

	context := OperatorContext{
		CurrentTangoDBStatefulSet: nil,
		DisablePolicies:           databaseds.Spec.DisablePolicies,
		EnableLoadBalancer:        databaseds.Spec.EnableLoadBalancer,
		Namespace:                 databaseds.Namespace,
		Name:                      databaseds.Name,
		ServiceType:               "ClusterIP",
		Port:                      databaseds.Spec.Port,
		DBPort:                    3306,
		ClusterDomain:             databaseds.Spec.ClusterDomain,
		DDSImage:                  databaseds.Spec.DDSImage,
		DBImage:                   databaseds.Spec.DBImage,
		DBDb:                      databaseds.Spec.DBDb,
		DBRootPw:                  databaseds.Spec.DBRootPw,
		DBUser:                    databaseds.Spec.DBUser,
		DBPassword:                databaseds.Spec.DBPassword,
		TangoDBStorageClass:       databaseds.Spec.TangoDBStorageClass,
		DatabaseDS:                databaseds.Spec.DatabaseDS,
		DirectConnection:          false,
		Script:                    "",
		Config:                    databaseds.Spec.Config,
		Args:                      "",
		ScriptType:                "",
		ScriptContents:            "",
		ConfigType:                "",
		ConfigContents:            "",
		MountedFile:               false,
		PullSecrets:               databaseds.Spec.PullSecrets,
		PullPolicy:                databaseds.Spec.ImagePullPolicy,
		NodeSelector:              databaseds.Spec.NodeSelector,
		Affinity:                  databaseds.Spec.Affinity,
		Tolerations:               databaseds.Spec.Tolerations,
		Resources:                 databaseds.Spec.Resources,
		VolumeMounts:              databaseds.Spec.VolumeMounts,
		Volumes:                   databaseds.Spec.Volumes,
		Env:                       databaseds.Spec.Env}

	if context.DatabaseDS == "" {
		context.DatabaseDS = "databaseds-" + databaseds.Name
	}
	if context.DBImage == "" {
		context.DBImage = DBImage
	}
	if context.DDSImage == "" {
		context.DDSImage = DDSImage
	}
	if context.PullPolicy == "" {
		context.PullPolicy = PullPolicy
	}
	if context.DBDb == "" {
		context.DBDb = "tango"
	}
	if context.DBRootPw == "" {
		context.DBRootPw = "tango"
	}
	if context.DBUser == "" {
		context.DBUser = "tango"
	}
	if context.DBPassword == "" {
		context.DBPassword = "tango"
	}

	log.Debugf("context: %+v", context)
	return context
}
