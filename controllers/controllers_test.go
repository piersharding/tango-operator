/*

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"fmt"
	"os"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	tangov1 "gitlab.com/piersharding/tango-operator/api/v1"
	apps "k8s.io/api/apps/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

var liveTest = false
var namespace = "default"
var databaseds_name = "testds"
var resource_name = "testds"
var test1_name = resource_name + "basic"

var _ = Context("Inside a new namespace", func() {
	ctx = context.TODO()
	// is this testing against an actual cluster ?
	if os.Getenv("TEST_USE_EXISTING_CLUSTER") == "true" {
		liveTest = true
	}

	Describe("Exercising TangoControls", func() {

		It("should create a new DatabaseDS resource with the specified name", func() {
			dbds := &tangov1.DatabaseDS{
				ObjectMeta: metav1.ObjectMeta{
					Name:      databaseds_name,
					Namespace: namespace,
				},
				Spec: tangov1.DatabaseDSSpec{
					DDSImage:           "registry.gitlab.com/piersharding/tango-operator/tango-databaseds:0.2.0",
					DBImage:            "registry.gitlab.com/piersharding/tango-operator/tango-db:0.2.0",
					ImagePullPolicy:    "IfNotPresent",
					EnableLoadBalancer: true,
				},
			}

			err := k8sClient.Create(ctx, dbds)
			Expect(err).NotTo(HaveOccurred(), "failed to create test DatabaseDS resource")

			// check the DBDS exists
			checkdbds := &tangov1.DatabaseDS{}
			Eventually(
				getResourceFunc(ctx, client.ObjectKey{Name: databaseds_name, Namespace: namespace}, checkdbds),
				time.Second*5, time.Millisecond*500).Should(BeNil())

			// if this is a live cluster test then we can expect real things to happen
			if liveTest {
				fmt.Fprintf(GinkgoWriter, "This is a test against a live cluster so wait for DatabaseDS\n")

				// check the TangoDB StatefulSet exists
				ss := &apps.StatefulSet{}
				Eventually(
					getResourceFunc(ctx, client.ObjectKey{Name: "databaseds-tangodb-" + databaseds_name, Namespace: namespace}, ss),
					time.Second*300, time.Millisecond*500).Should(BeNil())

				// check the DBDS StatefulSet exists
				Eventually(
					getResourceFunc(ctx, client.ObjectKey{Name: "databaseds-" + databaseds_name, Namespace: namespace}, ss),
					time.Second*300, time.Millisecond*500).Should(BeNil())
			}
		})

		It("should create a new DeviceServer resource with the specified name", func() {
			ds := &tangov1.DeviceServer{
				ObjectMeta: metav1.ObjectMeta{
					Name:      test1_name,
					Namespace: namespace,
				},
				Spec: tangov1.DeviceServerSpec{
					DatabaseDS:      databaseds_name,
					Image:           "registry.gitlab.com/piersharding/tango-operator/tango-examples:0.2.0",
					ImagePullPolicy: "IfNotPresent",
					Args:            "test",
					Config:          "https://gitlab.com/ska-telescope/ska-tango-examples/-/raw/master/charts/ska-tango-examples/data/configuration.old.json",
					Script:          "https://gitlab.com/ska-telescope/ska-tango-examples/-/raw/master/src/ska_tango_examples/basic_example/powersupply.py",
				},
			}
			// if this is not against a live cluster, then fake a manual TANGO_HOST
			if !liveTest {
				fmt.Fprintf(GinkgoWriter, "This is NOT a live cluster, so set DirectConnection\n")
				ds.Spec.DirectConnection = true
			}
			err := k8sClient.Create(ctx, ds)
			Expect(err).NotTo(HaveOccurred(), "failed to create test DeviceServer resource")

			// check the DS exists
			checkds := &tangov1.DeviceServer{}
			Eventually(
				getResourceFunc(ctx, client.ObjectKey{Name: test1_name, Namespace: namespace}, checkds),
				time.Second*5, time.Millisecond*500).Should(BeNil())

			if liveTest {
				fmt.Fprintf(GinkgoWriter, "This is a test against a live cluster so wait for DeviceServer\n")

				// check the DS StatefulSet exists
				ss := &apps.StatefulSet{}
				Eventually(
					getResourceFunc(ctx, client.ObjectKey{Name: "deviceserver-" + test1_name, Namespace: namespace}, ss),
					time.Second*300, time.Millisecond*500).Should(BeNil())

				Expect(*ss.Spec.Replicas).To(Equal(int32(1)))
			}
		})

	})
})

func getResourceFunc(ctx context.Context, key client.ObjectKey, obj client.Object) func() error {
	return func() error {
		return k8sClient.Get(ctx, key, obj)
	}
}

func getDeploymentReplicasFunc(ctx context.Context, key client.ObjectKey) func() int32 {
	return func() int32 {
		depl := &apps.Deployment{}
		err := k8sClient.Get(ctx, key, depl)
		Expect(err).NotTo(HaveOccurred(), "failed to get Deployment resource")

		return *depl.Spec.Replicas
	}
}
