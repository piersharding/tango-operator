/*

Copyright 2022 Piers Harding
Copyright 2022 SKA Observatory

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package controllers

import (
	"crypto/sha256"
	"fmt"
	"reflect"

	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"gitlab.com/piersharding/tango-operator/models"
	"gitlab.com/piersharding/tango-operator/utils"

	"context"

	ctrl "sigs.k8s.io/controller-runtime"

	tangov1 "gitlab.com/piersharding/tango-operator/api/v1"
	dtypes "gitlab.com/piersharding/tango-operator/types"
)

// Check and set the ConfigMap for DatabaseDS
func (r *DatabaseDSReconciler) checkSetConfigMap(ctx context.Context, req ctrl.Request, dcontext *dtypes.OperatorContext, databaseds *tangov1.DatabaseDS) (ctrl.Result, error) {
	var currentConfig *corev1.ConfigMap

	log := r.Log.WithValues("databaseds", req.NamespacedName)

	currentConfig, _ = r.getConfig(databaseds.Namespace, "databaseds-configs-"+databaseds.Name)

	// check Config - is it JSON, file or URL
	configContents, configMountedFile, err := utils.CheckConfig(dcontext.Config)
	if err != nil {
		Errorf(log, err, "DatabaseDSJob config is invalid: %s", err.Error())
		r.Recorder.Eventf(databaseds, corev1.EventTypeWarning, "Failed", "DatabaseDSJob config is invalid: %q", databaseds.Name)
		databaseds.Status.State = fmt.Sprintf("DatabaseDSConfigs invalid: %s\n", err.Error())
		return ctrl.Result{Requeue: false, RequeueAfter: 0}, fmt.Errorf("DatabaseDSJob config is invalid: %s", err.Error())
	}
	dcontext.ConfigContents = configContents
	_ = configMountedFile

	// https://stackoverflow.com/questions/37317003/restart-pods-when-configmap-updates-in-kubernetes
	// create dependent ConfigMap
	configMap, err := models.DatabaseDSConfigs(*dcontext)
	if err != nil {
		Errorf(log, err, "DatabaseDSConfigs Error: %+v\n", err)
		databaseds.Status.State = fmt.Sprintf("DatabaseDSConfigs Error: %+v\n", err)
		return ctrl.Result{}, err
	}
	configMap.ObjectMeta.OwnerReferences = []metav1.OwnerReference{*metav1.NewControllerRef(databaseds, tangov1.GroupVersion.WithKind("DatabaseDS"))}

	// set the reference
	if err := ctrl.SetControllerReference(databaseds, configMap, r.Scheme); err != nil {
		Errorf(log, err, "DatabaseDSConfigs Error: %+v\n", err)
		databaseds.Status.State = fmt.Sprintf("DatabaseDSConfigs Error: %+v\n", err)
		return ctrl.Result{}, err
	}

	Debugf(log, "DatabaseDSConfigs: %+v", *configMap)
	dcontext.ConfigCheckSum = fmt.Sprintf("%x", sha256.Sum256([]byte(fmt.Sprintf("%+v", configMap.Data))))

	if currentConfig == nil {
		Debugf(log, "###### Create ConfigMap #######")

		// create cluster wide ServiceAccount
		serviceAccount, err := models.DatabaseDSServiceAccount(*dcontext)
		if err != nil {
			Errorf(log, err, "DatabaseDSServiceAccount Error: %+v\n", err)
			databaseds.Status.State = fmt.Sprintf("DatabaseDSServiceAccount Error: %+v\n", err)
			return ctrl.Result{}, err
		}
		serviceAccount.ObjectMeta.OwnerReferences = []metav1.OwnerReference{*metav1.NewControllerRef(databaseds, tangov1.GroupVersion.WithKind("DatabaseDS"))}
		Debugf(log, "DatabaseDSServiceAccount: %+v", *serviceAccount)
		// set the reference
		if err := ctrl.SetControllerReference(databaseds, serviceAccount, r.Scheme); err != nil {
			Errorf(log, err, "DatabaseDSServiceAccount Error: %+v\n", err)
			databaseds.Status.State = fmt.Sprintf("DatabaseDSServiceAccount Error: %+v\n", err)
			return ctrl.Result{}, err
		}
		// ...and create it on the cluster
		if err := r.Create(ctx, serviceAccount); err != nil {
			log.Error(err, "unable to create DatabaseDSServiceAccount for DatabaseDS", "ServiceAccount", serviceAccount)
			databaseds.Status.State = fmt.Sprintf("unable to create DatabaseDSServiceAccount Error: %+v\n", err)
			return ctrl.Result{}, err
		}

		// create ConfigMap
		Infof(log, "############################# DatabaseDSConfigs: %+v", dcontext)
		// ...and create it on the cluster
		if err := r.Create(ctx, configMap); err != nil {
			log.Error(err, "unable to create ConfigMap for DatabaseDS", "configMap", configMap)
			databaseds.Status.State = fmt.Sprintf("unable to create ConfigMap Error: %+v\n", err)
			return ctrl.Result{}, err
		}
	} else {
		// Do we need to Update the ConfigMap ?
		if !reflect.DeepEqual(configMap.Data, currentConfig.Data) {
			err = r.Update(ctx, configMap)
			if err != nil {
				log.Error(err, "unable to update ConfigMap for DatabaseDS", "configMap", configMap)
				databaseds.Status.State = fmt.Sprintf("unable to update ConfigMap Error: %+v\n", err)
				return ctrl.Result{}, err
			}
		}

	}

	return ctrl.Result{}, nil
}

// Check and set the StatefulSet for the TangoDB
func (r *DatabaseDSReconciler) checkSetTangoDB(ctx context.Context, req ctrl.Request, dcontext *dtypes.OperatorContext, databaseds *tangov1.DatabaseDS) (ctrl.Result, error) {

	var currentTangoDBStatefulSet *appsv1.StatefulSet
	var currentTangoDBPVC *corev1.PersistentVolumeClaim

	log := r.Log.WithValues("databaseds", req.NamespacedName)

	currentTangoDBStatefulSet, _ = r.getStatefulSet(databaseds.Namespace, "databaseds-tangodb-"+databaseds.Name, databaseds)
	currentTangoDBPVC, _ = r.getPVC(databaseds.Namespace, "databaseds-tangodb-pvc-"+databaseds.Name)
	dcontext.CurrentTangoDBStatefulSet = currentTangoDBStatefulSet

	// create StatefulSet
	databaseDSStatefulSet, err := models.DatabaseDSTangoDBStatefulSet(dcontext.ForDatabaseDS())
	if err != nil {
		Errorf(log, err, "DatabaseDSTangoDBStatefulSet Error: %+v\n", err)
		databaseds.Status.State = fmt.Sprintf("DatabaseDSTangoDBStatefulSet Error: %+v\n", err)
		return ctrl.Result{}, err
	}
	databaseDSStatefulSet.ObjectMeta.OwnerReferences = []metav1.OwnerReference{*metav1.NewControllerRef(databaseds, tangov1.GroupVersion.WithKind("DatabaseDS"))}
	Debugf(log, "DatabaseDSTangoDBStatefulSet: %+v", *databaseDSStatefulSet)
	// set the reference
	if err := ctrl.SetControllerReference(databaseds, databaseDSStatefulSet, r.Scheme); err != nil {
		Errorf(log, err, "DatabaseDSTangoDBStatefulSet Error: %+v\n", err)
		return ctrl.Result{}, err
	}

	// create device server statefulset and service
	Debugf(log, "###### Create TangoDB #######")
	if currentTangoDBStatefulSet == nil {

		// create Service
		databaseDSTangoDBService, err := models.DatabaseDSTangoDBService(*dcontext)
		if err != nil {
			Errorf(log, err, "DatabaseDSTangoDBService Error: %+v\n", err)
			databaseds.Status.State = fmt.Sprintf("DatabaseDSTangoDBService Error: %+v\n", err)
			return ctrl.Result{}, err
		}
		databaseDSTangoDBService.ObjectMeta.OwnerReferences = []metav1.OwnerReference{*metav1.NewControllerRef(databaseds, tangov1.GroupVersion.WithKind("DatabaseDS"))}
		Debugf(log, "DatabaseDSTangoDBService: %+v", *databaseDSTangoDBService)
		// set the reference
		if err := ctrl.SetControllerReference(databaseds, databaseDSTangoDBService, r.Scheme); err != nil {
			Errorf(log, err, "DatabaseDSTangoDBService Error: %+v\n", err)
			return ctrl.Result{}, err
		}
		// ...and create it on the cluster
		if err := r.Create(ctx, databaseDSTangoDBService); err != nil {
			log.Error(err, "unable to create Service for DatabaseDS TangoDB", "Service", databaseDSTangoDBService)
			return ctrl.Result{}, err
		}

		// Create the PVC
		if currentTangoDBPVC == nil {

			databasedsTangoDBPVC, err := models.DatabaseDSTangoDBStorage(*dcontext)
			if err != nil {
				Errorf(log, err, "DatabaseDSTangoDBStorage Error: %+v\n", err)
				databaseds.Status.State = fmt.Sprintf("DatabaseDSTangoDBStorage Error: %+v\n", err)
				return ctrl.Result{}, err
			}
			databasedsTangoDBPVC.ObjectMeta.OwnerReferences = []metav1.OwnerReference{*metav1.NewControllerRef(databaseds, tangov1.GroupVersion.WithKind("DatabaseDS"))}
			Debugf(log, "DatabaseDSTangoDBStorage: %+v", *databasedsTangoDBPVC)
			// set the reference
			if err := ctrl.SetControllerReference(databaseds, databasedsTangoDBPVC, r.Scheme); err != nil {
				Errorf(log, err, "DatabaseDSTangoDBStorage Error: %+v\n", err)
				return ctrl.Result{}, err
			}
			// ...and create it on the cluster
			if err := r.Create(ctx, databasedsTangoDBPVC); err != nil {
				log.Error(err, "unable to create TangoDB PVC for DatabaseDS", "PVC", databasedsTangoDBPVC)
				return ctrl.Result{}, err
			}
			r.Recorder.Eventf(databaseds, corev1.EventTypeNormal, "Created", "Created TangoDB PVC %q", databasedsTangoDBPVC.Name)
		}
		// ...and create it on the cluster
		if err := r.Create(ctx, databaseDSStatefulSet); err != nil {
			log.Error(err, "unable to create StatefulSet for DatabaseDS", "StatefulSet", databaseDSStatefulSet)
			return ctrl.Result{}, err
		}
		r.Recorder.Eventf(databaseds, corev1.EventTypeNormal, "Created", "Created  deployment %q", databaseDSStatefulSet.Name)

		dcontext.CurrentTangoDBStatefulSet = databaseDSStatefulSet
	} else {
		// Do we need to Update the StatefulSet ?
		if !reflect.DeepEqual(currentTangoDBStatefulSet.ObjectMeta.Annotations, databaseDSStatefulSet.ObjectMeta.Annotations) {
			err = r.Update(ctx, databaseDSStatefulSet)
			if err != nil {
				log.Error(err, "unable to update StatefulSet for TangoDB", "databaseDSStatefulSet", databaseDSStatefulSet)
				databaseds.Status.State = fmt.Sprintf("unable to update StatefulSet Error: %+v\n", err)
				return ctrl.Result{}, err
			}
		}

	}
	return ctrl.Result{}, nil
}

// Check and set the StatefulSet for the DatabaseDS
func (r *DatabaseDSReconciler) checkSetDatabaseDS(ctx context.Context, req ctrl.Request, dcontext *dtypes.OperatorContext, databaseds *tangov1.DatabaseDS) (ctrl.Result, error) {

	var currentDSStatefulSet *appsv1.StatefulSet

	log := r.Log.WithValues("databaseds", req.NamespacedName)

	currentDSStatefulSet, _ = r.getStatefulSet(databaseds.Namespace, "databaseds-ds-"+databaseds.Name, databaseds)

	// create DatabaseDS component
	Debugf(log, "###### Create DS #######")

	// create StatefulSet
	dsStatefulSet, err := models.DatabaseDSStatefulSet(dcontext.ForDatabaseDS())
	if err != nil {
		Errorf(log, err, "DatabaseDSStatefulSet Error: %+v\n", err)
		databaseds.Status.State = fmt.Sprintf("DatabaseDSStatefulSet Error: %+v\n", err)
		return ctrl.Result{}, err
	}
	dsStatefulSet.ObjectMeta.OwnerReferences = []metav1.OwnerReference{*metav1.NewControllerRef(databaseds, tangov1.GroupVersion.WithKind("DatabaseDS"))}
	Debugf(log, "DatabaseDSStatefulSet: %+v", *dsStatefulSet)
	// set the reference
	if err := ctrl.SetControllerReference(databaseds, dsStatefulSet, r.Scheme); err != nil {
		Errorf(log, err, "DatabaseDSStatefulSet Error: %+v\n", err)
		return ctrl.Result{}, err
	}

	// only do this if the DB is up and running
	if dcontext.CurrentTangoDBStatefulSet != nil {
		Debugf(log, "currentTangoDBStatefulSet.Status.ReadyReplicas: %d - currentTangoDBStatefulSet.Status.Replicas: %d \n", dcontext.CurrentTangoDBStatefulSet.Status.ReadyReplicas, dcontext.CurrentTangoDBStatefulSet.Status.Replicas)
		if dcontext.CurrentTangoDBStatefulSet.Status.ReadyReplicas > 0 {
			Debugf(log, "###### TangoDB is UP !!!!NOW!!!! Create DS #######")

			if currentDSStatefulSet == nil {

				// create Service
				if dcontext.EnableLoadBalancer {
					dcontext.ServiceType = "LoadBalancer"
				}
				// if !dcontext.EnableLoadBalancer {
				Debugf(log, "###### Create DatabaseDSService #######")
				databaseDSService, err := models.DatabaseDSService(*dcontext)
				if err != nil {
					Errorf(log, err, "DatabaseDSService Error: %+v\n", err)
					databaseds.Status.State = fmt.Sprintf("DatabaseDSService Error: %+v\n", err)
					return ctrl.Result{}, err
				}
				databaseDSService.ObjectMeta.OwnerReferences = []metav1.OwnerReference{*metav1.NewControllerRef(databaseds, tangov1.GroupVersion.WithKind("DatabaseDS"))}
				Debugf(log, "DatabaseDSService: %+v", *databaseDSService)
				// set the reference
				if err := ctrl.SetControllerReference(databaseds, databaseDSService, r.Scheme); err != nil {
					Errorf(log, err, "DatabaseDSService Error: %+v\n", err)
					return ctrl.Result{}, err
				}
				// ...and create it on the cluster
				if err := r.Create(ctx, databaseDSService); err != nil {
					log.Error(err, "unable to create Service for DatabaseDS", "Service", databaseDSService)
					return ctrl.Result{}, err
				}

				// ...and create it on the cluster
				if err := r.Create(ctx, dsStatefulSet); err != nil {
					log.Error(err, "unable to create StatefulSet for DatabaseDS", "StatefulSet", dsStatefulSet)
					return ctrl.Result{}, err
				}
				r.Recorder.Eventf(databaseds, corev1.EventTypeNormal, "Created", "Created DS deployment %q", dsStatefulSet.Name)
				currentDSStatefulSet = dsStatefulSet
			}
		}
	} else {
		// Do we need to Update the StatefulSet based on the ConfigMap signature change ?
		if !reflect.DeepEqual(currentDSStatefulSet.ObjectMeta.Annotations, dsStatefulSet.ObjectMeta.Annotations) {
			err = r.Update(ctx, dsStatefulSet)
			if err != nil {
				log.Error(err, "unable to update StatefulSet for DatabaseDS", "dsStatefulSet", dsStatefulSet)
				databaseds.Status.State = fmt.Sprintf("unable to update StatefulSet Error: %+v\n", err)
				return ctrl.Result{}, err
			}
		}

	}
	return ctrl.Result{}, nil
}
