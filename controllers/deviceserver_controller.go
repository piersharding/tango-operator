/*

Copyright 2022 Piers Harding
Copyright 2022 SKA Observatory

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package controllers

import (
	"bytes"
	"errors"
	"fmt"
	"math/rand"
	"os"
	"os/exec"
	"regexp"
	"strings"
	"time"

	"context"

	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/tools/record"

	"github.com/go-logr/logr"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"

	tangov1 "gitlab.com/piersharding/tango-operator/api/v1"
	"gitlab.com/piersharding/tango-operator/utils"

	dtypes "gitlab.com/piersharding/tango-operator/types"
)

var (
	deviceServerOwnerKey = ".metadata.deviceservercontroller"
	deviceServerApiGVStr = tangov1.GroupVersion.String()
)

var validDeviceFQDN = regexp.MustCompile(`^([\w\-\.]+:\d+\/)?\w+\/\w+\/\w+`)

// +kubebuilder:rbac:groups=tango.tango-controls.org,resources=deviceservers,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=tango.tango-controls.org,resources=deviceservers/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=apps,resources=statefulsets,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=apps,resources=statefulsets/status,verbs=get
// +kubebuilder:rbac:groups=apps,resources=deployments,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=apps,resources=deployments/status,verbs=get
// +kubebuilder:rbac:groups=core,resources=configmaps;secrets;services;serviceaccounts,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=networking.k8s.io,resources=networkpolicies,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=services/status,verbs=get
// +kubebuilder:rbac:groups=extensions,resources=ingresses,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=extensions,resources=ingresses/status,verbs=get

// DeviceServerReconciler reconciles a DeviceServer object
type DeviceServerReconciler struct {
	client.Client
	Log       logr.Logger
	Scheme    *runtime.Scheme
	CustomLog dtypes.CustomLogger
	Recorder  record.EventRecorder
}

func (r *DeviceServerReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := r.Log.WithValues("deviceserver", req.NamespacedName)
	clog := r.CustomLog.WithValues("deviceserver", req.NamespacedName)
	_ = clog

	var deviceserver tangov1.DeviceServer
	if err := r.Get(ctx, req.NamespacedName, &deviceserver); err != nil {
		log.Info("unable to fetch DeviceServer(delete in progress?): " + err.Error())
		// we'll ignore not-found errors, since they can't be fixed by an immediate
		// requeue (we'll need to wait for a new notification), and we can get them
		// on deleted requests.
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}

	deviceserver.Status.Replicas = 0
	deviceserver.Status.Succeeded = 0
	deviceserver.Status.Resources = ""
	deviceserver.Status.State = "Building"

	var databaseds tangov1.DatabaseDS
	databasedsobjkey := client.ObjectKey{
		Namespace: req.Namespace,
		Name:      deviceserver.Spec.DatabaseDS,
	}
	if err := r.Get(ctx, databasedsobjkey, &databaseds); err != nil {
		log.Info("unable to fetch DatabaseDS(create/delete in progress?): " + err.Error())
		// we'll ignore not-found errors, since they can't be fixed by an immediate
		// requeue (we'll need to wait for a new notification), and we can get them
		// on deleted requests.
		deviceserver.Status.State = fmt.Sprintf("Pending creation of DatabaseDS cluster: %s", deviceserver.Spec.DatabaseDS)
		r.Status().Update(ctx, &deviceserver)
		return ctrl.Result{}, client.IgnoreNotFound(errors.New("unable to fetch DatabaseDS(create/delete in progress?): " + err.Error()))
	}

	// setup configuration.
	dcontext := dtypes.SetConfig(deviceserver)
	if dcontext.Image == "" {
		dcontext.Image = dtypes.Image
	}
	if dcontext.PullPolicy == "" {
		dcontext.PullPolicy = dtypes.PullPolicy
	}
	// this is a normal connection to a DatabaseDS resource, so set the hostname correctly
	if !dcontext.DirectConnection {
		dcontext.DatabaseDS = "databaseds-" + dcontext.DatabaseDS
	} else {
		// find the host and port
		tango_host := strings.SplitN(dcontext.DatabaseDS, ":", 2)
		dcontext.DatabaseDS = tango_host[0]
		if len(tango_host) > 1 {
			_, err := fmt.Sscan(tango_host[1], &dcontext.Port)
			// if parse failed then default to standard port 10000
			if err != nil {
				log.Info(fmt.Sprintf("failed to parse Port from DatabaseDS: %s - %s", req.NamespacedName, deviceserver.Spec.DatabaseDS))
				dcontext.Port = 10000
			}
		}
	}

	// if this is a DirectConnection (manually configured TANGO_HOST) then carry on
	// if this is not a DirectConnection and the DatabaseDS is not up correctly then delay until it is
	if !dcontext.DirectConnection && (databaseds.Status.State != "Running" || databaseds.Status.Succeeded != 2) {
		log.Info(fmt.Sprintf("DatabaseDS NOT ready: %s - %s - %s", deviceserver.Spec.DatabaseDS, databaseds.Status.State, databaseds.Status.Resources))
		// we'll ignore not-found errors, since they can't be fixed by an immediate
		// requeue (we'll need to wait for a new notification), and we can get them
		// on deleted requests.

		// set the status to waiting
		deviceserver.Status.State = "Waiting(" + deviceserver.Spec.DatabaseDS + ")"
		r.Status().Update(ctx, &deviceserver)
		// wait somewhere between 1 and 2 secs
		randmilisecs := rand.Intn(10)
		return ctrl.Result{RequeueAfter: time.Millisecond * time.Duration(1000+(randmilisecs*100))}, nil
	} else {
		log.Info(fmt.Sprintf("DatabaseDS IS ready: %s - %s - %s", deviceserver.Spec.DatabaseDS, databaseds.Status.State, databaseds.Status.Resources))
	}

	// if there is a dependency list then iterate over them to make sure they are up first
	if deviceserver.Spec.DependsOn != nil {
		var dependsOn []string
		for _, server := range deviceserver.Spec.DependsOn {

			var dependentDeviceServer tangov1.DeviceServer
			dependentDeviceServerobjkey := client.ObjectKey{
				Namespace: req.Namespace,
				Name:      server,
			}

			// "192.168.49.96:10000/sys/database/2" or
			//    "databaseds-testds.default.svc.cluster/local:10000/sys/database/2" or
			//    "sys/database/2"
			matched := validDeviceFQDN.MatchString(server)
			if matched {
				// We have a device FQDN - must use ping

				// if TangoAdmin is unset, then skip in-operator check
				if dtypes.TangoAdmin != "" {

					cmd := exec.Command(dtypes.TangoAdmin, "--ping-device", server)
					// set the default tango host
					var tango_host = fmt.Sprintf("TANGO_HOST=%s.%s.svc.cluster.local:%d", dcontext.DatabaseDS, req.Namespace, dcontext.Port)
					cmd.Env = append(os.Environ(),
						tango_host,
					)
					var out bytes.Buffer
					cmd.Stdout = &out
					if err := cmd.Run(); err != nil {
						log.Info(fmt.Sprintf("dependent DeviceServer IS NOT ready (err: %s - %s): %s/%s - %s - %s", err, out.String(), tango_host, server, dependentDeviceServer.Status.State, dependentDeviceServer.Status.Resources))
					}
					if cmd.ProcessState.ExitCode() != 0 {
						log.Info(fmt.Sprintf("dependent DeviceServer IS NOT ready (err: %s): %s - %s - %s", out.String(), server, dependentDeviceServer.Status.State, dependentDeviceServer.Status.Resources))

						// set the status to waiting
						deviceserver.Status.State = "Waiting..."
						r.Status().Update(ctx, &deviceserver)
						// wait somewhere between 1 and 3 secs
						randmilisecs := rand.Intn(20)
						return ctrl.Result{RequeueAfter: time.Millisecond * time.Duration(1000+(randmilisecs*100))}, nil
					} else {
						log.Info(fmt.Sprintf("dependent DeviceServer IS ready: %s - %s - %s", server, dependentDeviceServer.Status.State, dependentDeviceServer.Status.Resources))
					}
				} else {
					// defer the check to the initContainer dependson
					dependsOn = append(dependsOn, server)
				}
			} else {
				if err := r.Get(ctx, dependentDeviceServerobjkey, &dependentDeviceServer); err != nil {
					log.Info("unable to fetch dependent DeviceServer(create/delete in progress?): " + err.Error())
					// we'll ignore not-found errors, since they can't be fixed by an immediate
					// requeue (we'll need to wait for a new notification), and we can get them
					// on deleted requests.
					deviceserver.Status.State = "Waiting?"
					r.Status().Update(ctx, &deviceserver)
					// wait somewhere between 1 and 2 secs
					randmilisecs := rand.Intn(10)
					return ctrl.Result{RequeueAfter: time.Millisecond * time.Duration(1000+(randmilisecs*100))}, nil
				}

				if dependentDeviceServer.Status.State != "Running" {
					log.Info(fmt.Sprintf("dependent DeviceServer NOT ready: %s - %s - %s", server, dependentDeviceServer.Status.State, dependentDeviceServer.Status.Resources))
					// we'll ignore not-found errors, since they can't be fixed by an immediate
					// requeue (we'll need to wait for a new notification), and we can get them
					// on deleted requests.

					// set the status to waiting
					deviceserver.Status.State = "Waiting..."
					r.Status().Update(ctx, &deviceserver)
					// wait somewhere between .5 and 1 secs
					randmilisecs := rand.Intn(5)
					return ctrl.Result{RequeueAfter: time.Millisecond * time.Duration(1000+(randmilisecs*100))}, nil
				} else {
					log.Info(fmt.Sprintf("dependent DeviceServer IS ready: %s - %s - %s", server, dependentDeviceServer.Status.State, dependentDeviceServer.Status.Resources))
				}
			}
		}
		// reassign with only the remote pings
		dcontext.DependsOn = dependsOn
	}

	var childStatefulSets appsv1.StatefulSetList
	if err := r.List(ctx, &childStatefulSets, client.InNamespace(req.Namespace), client.MatchingFields{deviceServerOwnerKey: req.Name}); err != nil {
		log.Error(err, "unable to list child StatefulSets")
		if client.IgnoreNotFound(err) != nil {
			return ctrl.Result{}, err
		}
	}

	var (
		childSS      int = 0
		childSSready int = 0
	)
	// make sure that the children exist and that the status is correct
	for _, sschld := range childStatefulSets.Items {
		childSS += 1
		if sschld.Status.ReadyReplicas > 0 {
			childSSready += 1
		}
	}

	Debugf(log, "incoming context: %+v", deviceserver)

	// check Config - is it JSON, file or URL
	configContents, configMountedFile, err := utils.CheckConfig(dcontext.Config)
	if err != nil {
		Errorf(log, err, "DeviceServer config is invalid: %s", err.Error())
		r.Recorder.Eventf(&deviceserver, corev1.EventTypeWarning, "Failed", "DeviceServer config is invalid: %q", deviceserver.Name)
		return ctrl.Result{Requeue: false, RequeueAfter: 0}, fmt.Errorf("DeviceServer config is invalid: %s", err.Error())
	}

	// Now, apply the config directly
	// if Json2Tango is unset, then skip in-operator configuration
	if dtypes.Json2Tango != "" {

		dcontext.ConfigContents = ""
		configFile, err := os.CreateTemp("", "json2tango")
		if err != nil {
			Errorf(log, err, "Unable to allocate tempfile for json2tango")
			r.Recorder.Eventf(&deviceserver, corev1.EventTypeWarning, "Failed", "DeviceServer Unable to allocate tempfile for json2tango: %q", deviceserver.Name)
			return ctrl.Result{Requeue: false, RequeueAfter: 0}, fmt.Errorf("DeviceServer Unable to allocate tempfile for json2tango: %s", err.Error())
		}
		defer os.Remove(configFile.Name())
		if _, err := configFile.Write([]byte(configContents)); err != nil {
			Errorf(log, err, "Unable to write to tempfile for json2tango")
			r.Recorder.Eventf(&deviceserver, corev1.EventTypeWarning, "Failed", "DeviceServer Unable to write to tempfile for json2tango: %q", deviceserver.Name)
			return ctrl.Result{Requeue: false, RequeueAfter: 0}, fmt.Errorf("DeviceServer Unable to write to tempfile for json2tango: %s", err.Error())
		}
		if err := configFile.Close(); err != nil {
			Errorf(log, err, "Unable to close tempfile for json2tango")
			r.Recorder.Eventf(&deviceserver, corev1.EventTypeWarning, "Failed", "DeviceServer Unable to close tempfile for json2tango: %q", deviceserver.Name)
			return ctrl.Result{Requeue: false, RequeueAfter: 0}, fmt.Errorf("DeviceServer Unable to close tempfile for json2tango: %s", err.Error())
		}

		cmd := exec.Command(dtypes.Json2Tango, "-w", "-a", "-u", configFile.Name())
		// set the default tango host
		var tango_host = fmt.Sprintf("TANGO_HOST=%s.%s.svc.cluster.local:%d", dcontext.DatabaseDS, req.Namespace, dcontext.Port)
		cmd.Env = append(os.Environ(),
			tango_host,
		)
		var out bytes.Buffer
		cmd.Stdout = &out
		if err := cmd.Run(); err != nil {
			// RC 2 is legitimate because it's an update
			if cmd.ProcessState.ExitCode() != 2 {
				Errorf(log, err, fmt.Sprintf("could not apply DeviceServer configuration (err: %s): %s/%s - %s - %s", out.String(), tango_host, deviceserver.Name, configFile.Name(), configContents))
				// set the status to waiting
				deviceserver.Status.State = "Waiting..."
				r.Status().Update(ctx, &deviceserver)
				// wait somewhere between 1 and 3 secs
				r.Recorder.Eventf(&deviceserver, corev1.EventTypeWarning, "Failed", "DeviceServer config error: %q", deviceserver.Name)
				randmilisecs := rand.Intn(20)
				return ctrl.Result{RequeueAfter: time.Millisecond * time.Duration(1000+(randmilisecs*100))}, nil
			}
		}
		if cmd.ProcessState.ExitCode() != 0 && cmd.ProcessState.ExitCode() != 2 {
			Errorf(log, err, fmt.Sprintf("could not apply DeviceServer configuration (RC: %d/%s): %s - %s - %s", cmd.ProcessState.ExitCode(), out.String(), deviceserver.Name, configFile.Name(), configContents))

			// set the status to error
			deviceserver.Status.State = "Error"
			r.Status().Update(ctx, &deviceserver)
			r.Recorder.Eventf(&deviceserver, corev1.EventTypeWarning, "Failed", "DeviceServer config is invalid: %q", deviceserver.Name)
			return ctrl.Result{Requeue: false, RequeueAfter: 0}, fmt.Errorf("DeviceServerJob config is invalid: %s", err.Error())
		} else {
			Infof(log, fmt.Sprintf("DeviceServer config applied: %s - %s", deviceserver.Name, out.String()))
		}
	} else {
		// need to set this so that it appears in the initContainer
		dcontext.ConfigContents = configContents
	}

	// check Script - is it a script, file or URL
	scriptName, scriptContents, scriptMountedFile, err := utils.CheckScript(dcontext.Script)
	if err != nil {
		Errorf(log, err, "DeviceServerJob script is invalid: %s", err.Error())
		r.Recorder.Eventf(&deviceserver, corev1.EventTypeWarning, "Failed", "DeviceServerJob script is invalid: %q", deviceserver.Name)
		return ctrl.Result{Requeue: false, RequeueAfter: 0}, fmt.Errorf("DeviceServerJob script is invalid: %s", err.Error())
	}
	Infof(log, "AppName before: %s", dcontext.AppName)
	dcontext.ScriptContents = scriptContents
	// set the script to execute
	Infof(log, "len(scriptContents): %d", len(scriptContents))
	if len(scriptContents) > 0 {
		dcontext.AppName = scriptName
	}
	Infof(log, "AppName after: %s", dcontext.AppName)

	_ = scriptMountedFile
	_ = configMountedFile

	// Get resource details
	resources, err := r.resourceDetails(dcontext)
	if err != nil {
		deviceserver.Status.State = resources
		return ctrl.Result{}, err
	}
	deviceserver.Status.Resources = resources

	// Generate desired children.

	// process the ConfigMap
	if res, err := r.checkSetConfigMap(ctx, req, &dcontext, &deviceserver); err != nil {
		if _ = r.Status().Update(ctx, &deviceserver); err != nil {
			Errorf(log, err, "unable to update DeviceServer status: %s", req.Name)
		}
		return res, err
	}

	// process the DeviceServer StatefulSet
	if res, err := r.checkSetDeviceServer(ctx, req, &dcontext, &deviceserver); err != nil {
		if _ = r.Status().Update(ctx, &deviceserver); err != nil {
			Errorf(log, err, "unable to update DeviceServer status: %s", req.Name)
		}
		return res, err
	}

	// Compute status based on latest observed state. Check the running child StatefulSet
	// deviceserver.Status.Replicas is automatically incremented when each of the StatefulSet are Ready
	// (don't know how this magic happens???)
	if len(dcontext.ExternalDeviceServer) > 0 ||
		(deviceserver.Status.Replicas == 1 &&
			deviceserver.Status.Replicas == deviceserver.Status.Succeeded &&
			childSS == 1 && childSSready == childSS) {
		deviceserver.Status.State = "Running"
	}
	Infof(log, "Status replicas: %d, succeeded: %d - Status: %s Resources: %s", deviceserver.Status.Replicas, deviceserver.Status.Succeeded, deviceserver.Status.State, deviceserver.Status.Resources)

	// set the status and go home
	if err := r.Status().Update(ctx, &deviceserver); err != nil {
		Errorf(log, err, "unable to update DeviceServer status: %s", req.Name)
		return ctrl.Result{}, err
	}

	return ctrl.Result{}, nil
}

func (r *DeviceServerReconciler) SetupWithManager(mgr ctrl.Manager) error {

	if err := mgr.GetFieldIndexer().IndexField(context.Background(), &appsv1.StatefulSet{}, deviceServerOwnerKey, func(rawObj client.Object) []string {
		// grab the StatefulSet object, extract the owner...
		deployment := rawObj.(*appsv1.StatefulSet)
		owner := metav1.GetControllerOf(deployment)
		if owner == nil {
			return nil
		}
		// ...make sure it's a DeviceServer ...
		if owner.APIVersion != deviceServerApiGVStr || owner.Kind != "DeviceServer" {
			return nil
		}

		// ...and if so, return it
		return []string{owner.Name}
	}); err != nil {
		return err
	}

	return ctrl.NewControllerManagedBy(mgr).
		For(&tangov1.DeviceServer{}).
		Owns(&appsv1.StatefulSet{}).
		Owns(&corev1.Service{}).
		Owns(&corev1.ConfigMap{}).
		Complete(r)
}
