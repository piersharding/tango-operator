/*

Copyright 2022 Piers Harding
Copyright 2022 SKA Observatory

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package controllers

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"strings"

	"github.com/go-logr/logr"
	tangov1 "gitlab.com/piersharding/tango-operator/api/v1"
	dtypes "gitlab.com/piersharding/tango-operator/types"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

// Errorf helper
func Errorf(log logr.Logger, err error, format string, a ...interface{}) {
	log.Error(err, fmt.Sprintf(format, a...))
}

// Infof helper
func Infof(log logr.Logger, format string, a ...interface{}) {
	log.Info(fmt.Sprintf(format, a...))
}

// Debugf helper
func Debugf(log logr.Logger, format string, a ...interface{}) {
	log.V(1).Info(fmt.Sprintf(format, a...))
}

// set ownership and reference
func setOwnerReferences(res metav1.Object, cntlr metav1.Object, parentKind string, s *runtime.Scheme, t metav1.TypeMeta, log logr.Logger) error {

	// set ownership
	res.SetOwnerReferences([]metav1.OwnerReference{*metav1.NewControllerRef(cntlr, tangov1.GroupVersion.WithKind(parentKind))})

	Debugf(log, "%s: %+v\n", t.GetObjectKind(), res)
	// set the reference
	if err := ctrl.SetControllerReference(cntlr, res, s); err != nil {
		Errorf(log, err, "%s Error: %+v\n", t.GetObjectKind(), err)
		return err
	}
	return nil
}

// read back the status info for the Service resource
func (r *DeviceServerReconciler) serviceStatus(dcontext dtypes.OperatorContext, name string) (string, error) {
	ctx := context.Background()

	objkey := client.ObjectKey{
		Namespace: dcontext.Namespace,
		Name:      name,
	}

	log := r.Log.WithValues("service", objkey)

	service := corev1.Service{}
	if err := r.Get(ctx, objkey, &service); err != nil {
		Debugf(log, "serviceStatus.Get Error: %+v\n", err.Error())
		return "", client.IgnoreNotFound(err)
	}
	owner := metav1.GetControllerOf(&service)
	if owner == nil {
		err := errors.New("serviceStatus.Get Error: owner empty")
		log.Error(err, "serviceStatus.Get Error: owner empty", owner)
		return "", err
	}
	// ...make sure it's a DeviceServer...
	if owner.APIVersion != deviceServerApiGVStr || owner.Kind != "DeviceServer" {
		err := errors.New("serviceStatus.Get Error: wrong kind/owner")
		log.Error(err, "serviceStatus.Get Error: wrong kind/owner", owner)
		return "", err
	}

	var ports []string
	for _, p := range service.Spec.Ports {
		ports = append(ports, fmt.Sprintf("%s/%d", p.Name, p.Port))
	}
	portList := fmt.Sprintf("%s := %s/%s, %s", service.Name, service.Spec.Type, service.Spec.ClusterIP, strings.Join(ports[:], ","))

	if len(service.Status.LoadBalancer.Ingress) > 0 {
		var lbs []string
		for _, ing := range service.Status.LoadBalancer.Ingress {
			lbs = append(lbs, ing.IP)
		}
		portList = portList + " LB: " + strings.Join(lbs[:], ",")
	}
	Debugf(log, "The service status: %s\n", portList)
	return portList, nil
}

// read back the status info for the Deployment resource
func (r *DeviceServerReconciler) deploymentStatus(dcontext dtypes.OperatorContext, name string) (string, error) {

	ctx := context.Background()

	objkey := client.ObjectKey{
		Namespace: dcontext.Namespace,
		Name:      name,
	}

	log := r.Log.WithValues("deployment", objkey)

	deployment := appsv1.Deployment{}
	if err := r.Get(ctx, objkey, &deployment); err != nil {
		Debugf(log, "deploymentStatus.Get Error: %+v\n", err.Error())
		return "", client.IgnoreNotFound(err)
	}
	owner := metav1.GetControllerOf(&deployment)
	if owner == nil {
		err := errors.New("deploymentStatus.Get Error: owner empty")
		log.Error(err, "deploymentStatus.Get Error: owner empty", owner)
		return "", err
	}
	// ...make sure it's a DeviceServer...
	if owner.APIVersion != deviceServerApiGVStr || owner.Kind != "DeviceServer" {
		err := errors.New("deploymentStatus.Get Error: wrong kind/owner")
		log.Error(err, "deploymentStatus.Get Error: wrong kind/owner", owner)
		return "", err
	}

	Debugf(log, "The deployment: %+v\n", deployment)
	status, err := json.Marshal(&deployment.Status)
	if err != nil {
		Errorf(log, err, "deploymentStatus.json Error: %+v\n", err.Error())
		return "", err
	}
	Debugf(log, "The deployment status: %s\n", string(status))
	return string(status), nil
}

// pull together the resource details
func (r *DeviceServerReconciler) resourceDetails(dcontext dtypes.OperatorContext) (string, error) {

	log := r.Log.WithName("resourceDetails")

	resS, err := r.deploymentStatus(dcontext, "deviceserver-"+dcontext.Name)
	deviceServerService, err := r.serviceStatus(dcontext, "deviceserver-"+dcontext.Name)
	if err != nil {
		Errorf(log, err, "serviceStatus DeviceServer Error: %+v\n", err)
		return fmt.Sprintf("serviceStatus DeviceServer Error: %+v\n", err), err
	}
	return fmt.Sprintf("DSService: %s\nDeviceServer: %s", deviceServerService, resS), nil
}

// look up one of the deployments
func (r *DeviceServerReconciler) getDeployment(namespace string, name string, deviceserver *tangov1.DeviceServer) (*appsv1.Deployment, error) {
	ctx := context.Background()
	log := r.Log.WithValues("looking for deployment", name)
	deploymentKey := client.ObjectKey{
		Namespace: namespace,
		Name:      name,
	}
	deployment := appsv1.Deployment{}
	if err := r.Get(ctx, deploymentKey, &deployment); err != nil {
		Debugf(log, "deployment.Get Error: %+v\n", err.Error())
		Infof(log, "deployment.Get not found: %+v\n", deploymentKey)
		return nil, err
	}
	deviceserver.Status.Replicas++
	if deployment.Status.ReadyReplicas == deployment.Status.Replicas {
		deviceserver.Status.Succeeded++
	}
	return &deployment, nil
}

// look up one of the StatefulSet
func (r *DeviceServerReconciler) getStatefulSet(namespace string, name string, deviceserver *tangov1.DeviceServer) (*appsv1.StatefulSet, error) {
	ctx := context.Background()
	log := r.Log.WithValues("looking for statefulset", name)
	statefulsetKey := client.ObjectKey{
		Namespace: namespace,
		Name:      name,
	}
	statefulset := appsv1.StatefulSet{}
	if err := r.Get(ctx, statefulsetKey, &statefulset); err != nil {
		Debugf(log, "statefulset.Get Error: %+v\n", err.Error())
		Infof(log, "statefulset.Get not found: %+v\n", statefulsetKey)
		return nil, err
	}
	deviceserver.Status.Replicas++
	if statefulset.Status.ReadyReplicas == statefulset.Status.Replicas {
		deviceserver.Status.Succeeded++
	}
	return &statefulset, nil
}

// look up one of the Service
func (r *DeviceServerReconciler) getService(namespace string, name string) (*corev1.Service, error) {
	ctx := context.Background()
	log := r.Log.WithValues("looking for statefulset", name)

	// look for the Service
	objkey := client.ObjectKey{
		Namespace: namespace,
		Name:      name,
	}
	service := corev1.Service{}
	if err := r.Get(ctx, objkey, &service); err != nil {
		Debugf(log, "service.Get Error: %+v\n", err.Error())
		Infof(log, "service.Get not found: %+v\n", objkey)
		return nil, err
	}
	return &service, nil
}

// read back the status info for the Config resource
func (r *DeviceServerReconciler) getConfig(namespace string, name string) (*corev1.ConfigMap, error) {
	ctx := context.Background()
	log := r.Log.WithValues("looking for config", name)
	objkey := client.ObjectKey{
		Namespace: namespace,
		Name:      name,
	}

	config := corev1.ConfigMap{}
	if err := r.Get(ctx, objkey, &config); err != nil {
		Debugf(log, "configStatus.Get Error: %+v\n", err.Error())
		Infof(log, "configStatus.Get not found: %+v\n", objkey)
		return nil, err
	}

	return &config, nil
}

// look up one of the StatefulSet
func (r *DatabaseDSReconciler) getStatefulSet(namespace string, name string, databaseds *tangov1.DatabaseDS) (*appsv1.StatefulSet, error) {
	ctx := context.Background()
	log := r.Log.WithValues("looking for statefulset", name)
	statefulsetKey := client.ObjectKey{
		Namespace: namespace,
		Name:      name,
	}
	statefulset := appsv1.StatefulSet{}
	if err := r.Get(ctx, statefulsetKey, &statefulset); err != nil {
		Debugf(log, "statefulset.Get Error: %+v\n", err.Error())
		Infof(log, "statefulset.Get not found: %+v\n", statefulsetKey)
		return nil, err
	}
	databaseds.Status.Replicas++
	if statefulset.Status.ReadyReplicas == statefulset.Status.Replicas {
		databaseds.Status.Succeeded++
	}
	return &statefulset, nil
}

// read back the status info for the Config resource
func (r *DatabaseDSReconciler) getConfig(namespace string, name string) (*corev1.ConfigMap, error) {
	ctx := context.Background()
	log := r.Log.WithValues("looking for config", name)
	objkey := client.ObjectKey{
		Namespace: namespace,
		Name:      name,
	}

	config := corev1.ConfigMap{}
	if err := r.Get(ctx, objkey, &config); err != nil {
		Debugf(log, "configStatus.Get Error: %+v\n", err)
		Infof(log, "configStatus.Get not found: %+v\n", objkey)
		return nil, err
	}

	return &config, nil
}

// read back the status info for the PVC resource
func (r *DatabaseDSReconciler) getPVC(namespace string, name string) (*corev1.PersistentVolumeClaim, error) {
	ctx := context.Background()
	log := r.Log.WithValues("looking for PVC", name)
	objkey := client.ObjectKey{
		Namespace: namespace,
		Name:      name,
	}

	pvc := corev1.PersistentVolumeClaim{}
	if err := r.Get(ctx, objkey, &pvc); err != nil {
		Debugf(log, "pvcStatus.Get Error: %+v\n", err)
		Infof(log, "pvcStatus.Get not found: %+v\n", objkey)
		return nil, err
	}

	return &pvc, nil
}

// read back the status info for the Service resource
func (r *DatabaseDSReconciler) serviceStatus(dcontext dtypes.OperatorContext, name string) (string, error) {
	ctx := context.Background()

	objkey := client.ObjectKey{
		Namespace: dcontext.Namespace,
		Name:      name,
	}

	log := r.Log.WithValues("service", objkey)

	service := corev1.Service{}
	if err := r.Get(ctx, objkey, &service); err != nil {
		Debugf(log, "serviceStatus.Get Error: %+v\n", err.Error())
		Infof(log, "serviceStatus.Get not found: %+v\n", objkey)
		return "", client.IgnoreNotFound(err)
	}
	owner := metav1.GetControllerOf(&service)
	if owner == nil {
		err := errors.New("serviceStatus.Get Error: owner empty")
		log.Error(err, "serviceStatus.Get Error: owner empty", owner)
		return "", err
	}
	// ...make sure it's a DatabaseDS...
	if owner.APIVersion != databaseDSApiGVStr || owner.Kind != "DatabaseDS" {
		err := errors.New("serviceStatus.Get Error: wrong kind/owner")
		log.Error(err, "serviceStatus.Get Error: wrong kind/owner", owner)
		return "", err
	}

	var ports []string
	for _, p := range service.Spec.Ports {
		ports = append(ports, fmt.Sprintf("%s/%d", p.Name, p.Port))
	}
	portList := fmt.Sprintf("%s := %s/%s, %s", service.Name, service.Spec.Type, service.Spec.ClusterIP, strings.Join(ports[:], ","))

	if len(service.Status.LoadBalancer.Ingress) > 0 {
		var lbs []string
		for _, ing := range service.Status.LoadBalancer.Ingress {
			lbs = append(lbs, ing.IP)
		}
		portList = portList + " LB: " + strings.Join(lbs[:], ",")
	}
	Debugf(log, "The service status: %s\n", portList)
	return portList, nil

}

// read back the status info for the StatefulSet resource
func (r *DatabaseDSReconciler) statefulsetStatus(dcontext dtypes.OperatorContext, name string) (string, error) {

	ctx := context.Background()

	objkey := client.ObjectKey{
		Namespace: dcontext.Namespace,
		Name:      name,
	}

	log := r.Log.WithValues("statefulset", objkey)

	statefulset := appsv1.StatefulSet{}
	if err := r.Get(ctx, objkey, &statefulset); err != nil {
		Debugf(log, "statefulsetStatus.Get Error: %+v\n", err.Error())
		Infof(log, "statefulsetStatus.Get not found: %+v\n", objkey)
		return "NotFound", client.IgnoreNotFound(err)
	}
	owner := metav1.GetControllerOf(&statefulset)
	if owner == nil {
		err := errors.New("statefulsetStatus.Get Error: owner empty")
		log.Error(err, "statefulsetStatus.Get Error: owner empty", owner)
		return "OwnerError", err
	}
	// ...make sure it's a DatabaseDS...
	if owner.APIVersion != databaseDSApiGVStr || owner.Kind != "DatabaseDS" {
		err := errors.New("statefulsetStatus.Get Error: wrong kind/owner")
		log.Error(err, "statefulsetStatus.Get Error: wrong kind/owner", owner)
		return "OwnerKindError", err
	}

	Debugf(log, "The statefulset: %+v\n", statefulset)
	return fmt.Sprintf("%d of %d", statefulset.Status.ReadyReplicas, statefulset.Status.Replicas), nil
}

// pull together the resource details
func (r *DatabaseDSReconciler) resourceDetails(dcontext dtypes.OperatorContext) (string, string, string, error) {

	resDB, _ := r.statefulsetStatus(dcontext, "databaseds-tangodb-"+dcontext.Name)
	resDS, _ := r.statefulsetStatus(dcontext, "databaseds-ds-"+dcontext.Name)
	resTangoDBService, _ := r.serviceStatus(dcontext, "databaseds-tangodb-"+dcontext.Name)
	resDSService, _ := r.serviceStatus(dcontext, "databaseds-"+dcontext.Name)

	return fmt.Sprintf("%s - %s", resTangoDBService, resDSService), resDB, resDS, nil
}
